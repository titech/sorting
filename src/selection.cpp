template <class T>
void SelectionSort(T * array,Index size)
{
	for (Index i =0; i<size-1;i++)
	{
		Index pos_min=i;
		//std::cerr<<i<<"\n";
		for (Index j=i;j<size;j++)
			{
				if (array[j]<array[pos_min]) pos_min=j;
			}
		std::swap(array[i],array[pos_min]);
	}
}

template <class T>
void InsertionSort(T * array,Index size)
{
	for (Index i=1; i<size; i++)
	{
		Index pos=i;
		T key=array[i];
		while ((pos>0)&&(array[pos-1]>key))
		{
			array[pos]=array[pos-1];
			pos--;
		}
		array[pos]=key;
	}
}