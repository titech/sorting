#include <fstream>
#include <string>
#include <iostream>
#include <map>
#include <algorithm>

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <sys/stat.h>
#include "utils/timer.hpp"

#include "sortutils.hpp"

//typedef unsigned long long datatype;


void FillRNDNums(unsigned long int * a, Index size, int seed)
{
    srand(seed);
    std::cerr<<"warning this version of FillRNDNums is deprecated\n";
    for (Index i=0;i<size;i++)
        a[i]= rand()%256;
}

std::string GetHostName()
{
	char hostname[1024];
	hostname[1023] = '\0';
	gethostname(hostname, 1023);
    std::string str_hostname = std::string(hostname,hostname+strlen(hostname));
    std::size_t found = str_hostname.find(".");
    if (found!=std::string::npos)
        str_hostname=str_hostname.substr(0,found);
	return str_hostname;
}

std::string GetExperimentName(long len_alphabet,long len_key,std::string name_compiler,std::string name_algo,long num_threads)
{
    std::string result="";
    char * places=getenv("OMP_PLACES");
    std::string str_places;
    if (places!=NULL) str_places=std::string(places);
    result=result+std::string("host:")+GetHostName()+std::string(",");
    result=result+std::string("compiler:")+name_compiler+std::string(",");
    result=result+std::string("omp_places:")+str_places+std::string(",");
    result=result+std::string("algo:")+name_algo+std::string(",");
    result=result+std::string("threads:")+FormatHelper::ConvertToStr(num_threads)+std::string(",");
    result=result+std::string("keylen:")+FormatHelper::ConvertToStr(len_key)+std::string(",");
    result=result+std::string("alphsize:")+FormatHelper::ConvertToStr(len_alphabet);
    return result;
}


void WriteStatsToFile(const std::string filename,Index cntReads, double keyPerSec)
{
    std::ofstream fo;
    std::string name=std::string("logs/")+filename;
    struct stat st;
    st.st_size=0;
    stat(name.c_str(), &st);
    size_t size = st.st_size;
    fo.open(name.c_str(),std::ofstream::out | std::ofstream::app);
    if (size==0)
    {
        fo<<"keys, throughput\n";
    }
    fo<<cntReads<<", "<<keyPerSec<<"\n";
    fo.close();
}

void StatWriter::Write(std::string name_experiment)
{
    std::ofstream fo;
    std::string name_file=std::string("logs/")+GetHostName()+std::string("_")+name_experiment;
    struct stat st;
    st.st_size=0;
    stat(name_file.c_str(), &st);
    size_t size = st.st_size;
    fo.open(name_file.c_str(),std::ofstream::out | std::ofstream::app);
    std::stringstream ss;
    if (size==0)
    {
        for ( auto i : stats )
            ss<<i.first<<" ";
        std::string s=ss.str();
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        fo<<s;
        fo<<"\n";
    }

    for ( auto i : stats )
        fo<<i.second<<" ";
    fo<<"\n";
    fo.close();
}

void StatWriter::Accumulate(std::string name_prop, double value)
{
    stats[name_prop]=value;
}

