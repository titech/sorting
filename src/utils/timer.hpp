#ifndef MY_TIMER
#define MY_TIMER


#include <map>

#include "utils.hpp"

#include <time.h>

//#define SECONDS_PER_DAY 86400.0

//typedef std::map<std::string, double, std::less<std::string> > TimeStats;


class Timer
{
private:

    double start, end;

public:
	Timer();
	void Notch();
	double GetLastIntervalSeconds();
	std::string GetLastIntervalHumanReadable();

	inline static double GetSeconds()
	{
		struct timespec mytimespec;
    	clock_gettime(CLOCK_REALTIME,&mytimespec);
    	return (static_cast<double>(mytimespec.tv_sec) + (mytimespec.tv_nsec / 1000000000.0));
	}

	static std::string SecondsToHumanStr(double seconds)
	{
		int newSpan;

	    if (seconds < 60)
		{
			return FormatHelper::ConvertToStr(seconds) + " seconds ";
		}
	    if (seconds < 60*60)
		{
			newSpan = static_cast<int>(seconds / 60.0);
			return FormatHelper::ConvertToStr(newSpan) + " minutes " + FormatHelper::ConvertToStr((int) seconds % 60) + " sec";
		}
	    if (seconds < 60*60*24)
		{
			newSpan = static_cast<int>(seconds / (60.0 * 60.0));
			return FormatHelper::ConvertToStr(newSpan) + " hours";
		}
	    if (seconds < 60*60*24*30)
		{
			newSpan = static_cast<int>(seconds / (60.0 * 60.0 * 24));
			return FormatHelper::ConvertToStr(newSpan) + " days";
		}
	    if (seconds < 60*60*24*30*12)
		{
			newSpan = static_cast<int>(seconds / (60.0 * 60.0 * 24 * 30));
			return FormatHelper::ConvertToStr(newSpan) + " months";
		}
		newSpan = static_cast<int>(seconds / (60.0 * 60.0 * 24 * 356));
		return FormatHelper::ConvertToStr(newSpan) + " years";
	}

	static std::string GetTomorrowTimeStr()
	{
		std::ostringstream out;
		out << (Timer::GetSeconds()+60*60*24);
		std::string str = out.str();
		return str;
	}

	static std::string GetCurrentTimeStr()
	{
		std::ostringstream out;
		out << Timer::GetSeconds();
		std::string str = out.str();
		return str;
	}

	static std::string GetTodayDateStr()
	{
		#if  defined(ARCH_PB) || defined(ARCH_PBEMU)
		char outstr[200];
		time_t t;
		struct tm *tmp;
		t = time(NULL);
		tmp = localtime(&t);
		if (tmp == NULL)
		{
			throw Exception("localtime");
		}

		if (strftime(outstr, sizeof(outstr),"%Y-%m-%d", tmp) == 0)
		{
			throw Exception("strftime returned 0");
		}
		//printf("Result string is \"%s\"\n", outstr);
		return std::string(outstr);
		#endif
		#if  defined(ARCH_WIN) || defined(ARCH_WM)
			//#warning GetTodayDateStr not emplemented
			return "not working yet";
		#endif

		return "not working!!!";

	}
};



#endif
