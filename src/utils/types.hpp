#ifndef MY_TYPES
#define MY_TYPES

#include <stdint.h>

//typedef unsigned long Index;
//typedef int64_t Index;
//typedef unsigned long long Index;
typedef unsigned long Index;
typedef unsigned char byte;

#define LENGTH_ALPHABET 255
#define IS_SENTINEL_APPENDED true

#define MAX_SORT_DEPTH 20
#define LEN_WORD 2
#define MIC_DEV 0
#define CUDA_DEV 0


struct RangeLight
{
	Index a,b;
	Index length;
	Index posStart;
};

class Range: public RangeLight
{
public:
	Range ()
	{
	    a=0;
	    b=0;
	    length=0;
	    posStart=0;
	}
	Range (Index _a,Index   _b)
	{
	    a=_a;
	    b=_b;
	}
} ;

#endif
