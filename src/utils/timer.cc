#include "timer.hpp"
#include <unistd.h>
#include <iostream>
#include <time.h>

Timer::Timer()
{
   end=GetSeconds();
   start = end;
}

void Timer::Notch()
{
	start=end;
    end=GetSeconds();
}

double Timer::GetLastIntervalSeconds()
{
    return end-start;
	//return ((double(clockStop - clockStart))/static_cast<double>(sysconf(_SC_CLK_TCK)));
	//return (static_cast<double>(clockStop - clockStart))/static_cast<double>(CLOCKS_PER_SEC);
}

std::string Timer::GetLastIntervalHumanReadable()
{
	return Timer::SecondsToHumanStr(GetLastIntervalSeconds());
}
