#include <cstdio>
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}


void PrintProperties(unsigned int numDevice)
{
	cudaDeviceProp devProp;
	cudaGetDeviceProperties ( &devProp, numDevice );
	fprintf (stderr, "Device #               : %u\n", numDevice );
	fprintf (stderr, "Name                   : %s\n", devProp.name );
	fprintf (stderr, "Compute capability     : %d.%d\n", devProp.major, devProp.minor );
	fprintf (stderr, "Total Global Memory    : %lu\n", devProp.totalGlobalMem );
	fprintf (stderr, "Shared memory per block: %lu\n", devProp.sharedMemPerBlock );
	fprintf (stderr, "Registers per block    : %d\n", devProp.regsPerBlock );
	fprintf (stderr, "Warp size              : %d\n", devProp.warpSize );
	fprintf (stderr, "Max threads per block  : %d\n", devProp.maxThreadsPerBlock );
	fprintf (stderr, "Total constant memory  : %lu\n", devProp.totalConstMem );
}


