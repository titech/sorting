__global__ void krnlEmpty()
{
    unsigned i=threadIdx.x;
}
__device__ void devSortStringRadixQuicksort( const Index start, const Index end, const Index depth, Index * sa, const byte * ref)
{
  if (start+1>=end) return;

  printf(" %ld",end-start);

    Index lo=start, hi=end;
    Index i=start;
    Index tmp;
    byte pivot = ref[sa[i]+depth];
    while (i<=hi)
    {
        if (ref[sa[i]+depth]<pivot)
        {
            tmp=sa[i]; sa[i]=sa[lo]; sa[lo]=tmp;
            lo++;
            i++;
        }else
        if (ref[sa[i]+depth]>pivot)
        {
            tmp=sa[i]; sa[i]=sa[hi]; sa[hi]=tmp;
            hi--;
        }else
        if (ref[sa[i]+depth]==pivot) i++;
        break;
    }
    return;
    if (start<lo) devSortStringRadixQuicksort(start,lo-1,depth, sa,ref);
    if (hi<end) devSortStringRadixQuicksort(hi+1,end,depth,sa,ref);
    if (depth<7) devSortStringRadixQuicksort(lo,hi,depth+1,sa,ref);
}


__global__ void krnlSortTasks(const unsigned int depth,Index * buckets, const Index cntBuckets,
                                    Index * sa, const byte * ref)
{
    unsigned long idThread = blockIdx.x * blockDim.x +threadIdx.x;
    if (idThread>=cntBuckets) return;
    devSortStringRadixQuicksort(buckets[idThread],buckets[idThread+1]-1,depth,sa,ref);
}

__global__ void krnlCountBucketsString(Index * gpuBucketsCnts, const Index* __restrict__ offsets, const byte* __restrict__ buf_data, const Index count)
{
    unsigned  long cntThreads = blockDim.x*gridDim.x;
    unsigned  long idThread = blockIdx.x * blockDim.x +threadIdx.x;
    double cntLinesPerThread=(double(count))/cntThreads;
    Index begin= (cntLinesPerThread*idThread);
    Index end  = (cntLinesPerThread*(idThread+1));

    if (cntThreads>=count)
    {
        if (idThread>=count) return;
        begin=idThread;
        end=idThread+1;
    }
    //if ((blockIdx.x==)&&())
    for (Index idLine=begin; idLine<end; idLine++)
    {
        Index state=0;
        unsigned long int tmp=*(reinterpret_cast<const unsigned long int *>(&(buf_data[offsets[idLine]])));
        for (Index idSymbol=0; idSymbol<LEN_WORD; idSymbol++)
        {
//            state = (state*LENGTH_ALPHABET+sa.buf_data[sa.offsets[idLine]+idSymbol]);
            state = state*LENGTH_ALPHABET+tmp % 256;
            tmp=tmp >>8;
        }
        atomicAdd(&(gpuBucketsCnts[state]), 1);
    }
  //  __syncthreads();
    //cudaDeviceSynchronize();
    //if (idThread==0)
    // krnlCalcOffsets<<<1,1>>>(gpuBucketsCnts,gpuBucketsOffsets,CNT_BUCKETS);
}

__global__ void krnlCalcOffsets(const Index  * gpuCntsPreffixes, Index  * gpuOffsets, Index  cntBuckets)
{
    long idThread = blockIdx.x * blockDim.x +threadIdx.x;
    if (idThread ==0)
    {
        gpuOffsets[0]=0;
        for (Index i=1; i<=cntBuckets; i++)
            gpuOffsets[i]=gpuOffsets[i-1]+gpuCntsPreffixes[i-1];
    }
}

__global__ void krnlBuildPointersByBuckets(Index  * gpuOffsets, Index * const sa,Index * sa2, byte * const ref,const Index cntLines)
{
    long idThread = blockIdx.x * blockDim.x +threadIdx.x;
    int cntThreads = blockDim.x*gridDim.x;
    double cntLinesPerThread=(double(cntLines))/cntThreads;
    Index begin= cntLinesPerThread*idThread;
    Index end  = cntLinesPerThread*(idThread+1);
    if (cntThreads>=cntLines)
    {
        if (idThread>=cntLines) return;
        begin=idThread;
        end=idThread+1;
    }
    for (Index idLine=begin; idLine<end; idLine++)
    {
        Index state=0;
        unsigned long int tmp=*(reinterpret_cast<unsigned long int *>(&(ref[sa[idLine]])));
        for (Index idSymbol=0; idSymbol<LEN_WORD; idSymbol++)
        {
//            state = (state*LENGTH_ALPHABET+sa.buf_data[sa.offsets[idLine]+idSymbol]);
            state = state*LENGTH_ALPHABET+tmp % 256;
            tmp=tmp >>8;
        }
        Index posSA=atomicAdd(&(gpuOffsets[state]), 1);
        sa2[posSA]=sa[idLine];
        //sa2[idLine]=sa[idLine];
    }
}

__global__ void kenlSortPref2Stage(const unsigned int depth, Index * buckets, const Index cntBuckets, Index * gpuBuckets2StageCnts,Index * gpuBuckets2StageOffs,
                                   Index * sa,Index * sa2, byte * bordersBuckets, const byte * ref)
{
   // unsigned  long cntThreads = blockDim.x*gridDim.x;
    //block id is Chunk id
    //threads in a block sort chubk together

    double cntBucketsPerBlock=(double(cntBuckets))/gridDim.x;
    Index beginBuck= (cntBucketsPerBlock*blockIdx.x);
    Index endBuck  = (cntBucketsPerBlock*(blockIdx.x+1));
    Index * ptrBucketsLocal =gpuBuckets2StageCnts+(cntBuckets+1)*blockIdx.x;
    Index * ptrBucketsLocalOffs  =gpuBuckets2StageOffs+(cntBuckets+1)*blockIdx.x;
    //printf("\nsecond stage from %lu to %lu\n",beginBuck,endBuck);
//    printf("count buckets= %lu\n",cntBuckets);
    for (Index idBuck=beginBuck; idBuck<endBuck; idBuck++)
    {
        //if (idThread==0)
        //  printf("\tsuf  stage from %lu to %lu\n",buckets[i],buckets[i+1]);
        if (threadIdx.x ==0)  // DO IT PARALLEL
        {
            for(Index i=0; i<cntBuckets; i++)
                ptrBucketsLocal[i]=0;
        }
        cudaDeviceSynchronize();

        Index cntLines=buckets[idBuck+1]-buckets[idBuck];
        //printf("\tsorting bucket %lu of %lu of %lu lines\n",i,cntBuckets,cntLines);
        double cntLinesPerThread=(double(cntLines))/blockDim.x;
        Index beginLines= (cntLinesPerThread*threadIdx.x)+buckets[idBuck];
        Index endLines  = cntLinesPerThread*(threadIdx.x+1)+buckets[idBuck];
        //printf("\tsuf  stage from %lu to %lu\n",beginLines,endLines);

        if (blockDim.x>=cntLines)
        {
            if (threadIdx.x>=cntLines) continue;
            beginLines=threadIdx.x+buckets[idBuck];
            endLines=threadIdx.x+1+buckets[idBuck];
        }
        for (Index idLine=beginLines; idLine<endLines; idLine++)
        {
            Index state=0;
            //printf("\tpasing line %lu of %lu \n",idLine,endLines);
            for (Index idSymbol=0; idSymbol<LEN_WORD; idSymbol++)
            {
                state = (state*LENGTH_ALPHABET+ref[sa[idLine]+idSymbol+depth]);
            }
            //  printf("\tstate= %lu\n",state);
            atomicAdd(&(ptrBucketsLocal[state]), 1);
            //break;
        }
        //break;
//        __syncthreads();
        cudaDeviceSynchronize();

        if (threadIdx.x ==0)
        {
            ptrBucketsLocalOffs[0]=buckets[idBuck];
            for(Index i=1; i<cntBuckets; i++)
                ptrBucketsLocalOffs[i]=ptrBucketsLocalOffs[i-1]+ptrBucketsLocal[i-1];
        }
//        __syncthreads();
        cudaDeviceSynchronize();

        for (Index idLine=beginLines; idLine<endLines; idLine++)
        {
            Index state=0;
            for (Index idSymbol=0; idSymbol<LEN_WORD; idSymbol++)
                state = (state*LENGTH_ALPHABET+ref[sa[idLine]+idSymbol+depth]);
            Index posSA=atomicAdd(&(ptrBucketsLocalOffs[state]), 1);
            sa2[posSA]=sa[idLine];
        }
        if (threadIdx.x ==0)
        {
            ptrBucketsLocalOffs[0]=buckets[idBuck];
            for(Index i=1; i<=cntBuckets; i++)
            {
                ptrBucketsLocalOffs[i]=ptrBucketsLocalOffs[i-1]+ptrBucketsLocal[i-1];
                bordersBuckets[ptrBucketsLocalOffs[i]]=1;
            }
            //    krnlSortStack<<<(cntBuckets+32-1)/32,32>>>(depth+LEN_WORD,ptrBucketsLocalOffs,CNT_BUCKETS,sa2,sa,bordersBuckets,ref,sizeRef,bufStack+SIZE_SORT_FRAME*blockIdx.x);
        }
//           cudaDeviceSynchronize();
        __syncthreads();

    }
}


__device__ void devSortStringsCountRecursive(const Index start, const Index end, const Index depth, Index * sa,Index * sa2, const byte * ref)
{
    if (start+1>=end) return;
    Index count_letters[LENGTH_ALPHABET]= {0};
    Index count_offsets[LENGTH_ALPHABET]= {0};

    for (Index pos=start; pos<end; pos++)
    {
        byte currentSymbol=ref[(sa[pos]+depth)];
        count_letters[currentSymbol]++;
    }
    for (int i=1; i<LENGTH_ALPHABET; i++)
        count_offsets[i]=count_offsets[i-1]+count_letters[i-1];
    for (Index idLine=start; idLine<end; idLine++)
    {
        byte currentSymbol=ref[(sa[idLine]+depth)];
        sa2[start+(count_offsets[currentSymbol])++]=sa[idLine];
    }
    for (Index idLine=start; idLine<end; idLine++)
        sa[idLine]=sa2[idLine];
    if (depth<8)
    {
        for (int i=0; i<LENGTH_ALPHABET; i++)
        {
            if(count_letters[i]>1)
            {
                devSortStringsCountRecursive((i==0?0:count_offsets[i-1])+start,count_offsets[i]+start,depth+1,sa,sa2, ref);
            }
            //else
            //bordersBuckets[(i==0?0:count_offsets[i-1])+scope.start]=1;
        }
    }
}


__global__ void krnlSortRecursive(const unsigned int depth,Index * buckets, const Index cntBuckets,
                                    Index * sa, Index * sa2, const byte * ref)
{
    unsigned long idThread = blockIdx.x * blockDim.x +threadIdx.x;
    if (idThread>=cntBuckets) return;
    //if (idThread>cntBuckets-10)
    //    printf("kernel %lu rev sort from %lu to %lu \n",idThread, buckets[idThread], buckets[idThread+1]);
    //devSortStringRadixQuicksort(buckets[idThread],buckets[idThread+1],depth,sa,ref);
    devSortStringsCountRecursive(buckets[idThread],buckets[idThread+1],depth,sa,sa2,ref);
}

