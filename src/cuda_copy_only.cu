#include <iostream>
#include <cuda.h>
#include "sortutils.hpp"
#include "utils/mycuda.hpp"

void SortCudaFake(datatype * array,Index size)
{
	//create GPU buf
	datatype * gpuArray = NULL;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);

	gpuErrchk( cudaMalloc(&gpuArray, size*sizeof(datatype)) );
	gpuErrchk( cudaMemcpy(gpuArray,array, size*sizeof(datatype), cudaMemcpyHostToDevice) );
	gpuErrchk( cudaMemcpy(array, gpuArray, size*sizeof(datatype), cudaMemcpyDeviceToHost) );

	gpuErrchk( cudaFree(gpuArray) );
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	//copy to device
	//copy to host
	//free gpu buf
}

int main(int argc, char* argv[])
{
    int seed=1;
    for (Index size=1ll*1000*100;size<20ll*1000*1000;size+=1ll*1000*30)
    {
		datatype * array = new datatype [size];
        RunExperiment(SortCudaFake,array,size,"gpu_fake",seed);
	    delete [] array;
    }
    
}