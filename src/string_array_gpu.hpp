#include "string_array.hpp"

class GPUStringArray: public StringArray
{
	public:

	void Allocate(const StringArray & sa)
	{
		count= sa.count;
		gpuErrchk( cudaMalloc(&offsets, count*sizeof(Index)) );
		gpuErrchk( cudaMalloc(&buf_data, sa.offsets[count]) );
	}
	void CopyToGPU(const StringArray & sa)
	{
		gpuErrchk( cudaMemcpy(offsets,sa.offsets, count*sizeof(Index), cudaMemcpyHostToDevice) );
		gpuErrchk( cudaMemcpy(buf_data,sa.buf_data, sa.offsets[count], cudaMemcpyHostToDevice) );
	}
	void CopyFromGPU(StringArray & sa)
	{
		gpuErrchk( cudaMemcpy(sa.offsets,offsets, count*sizeof(Index), cudaMemcpyDeviceToHost) );
		//gpuErrchk( cudaMemcpy(sa.bufData,bufData, sa.offsets[count], cudaMemcpyHostToDevice) );
	}
	void Free()
	{
		gpuErrchk( cudaFree(offsets) );
		gpuErrchk( cudaFree(buf_data) );
	}
};