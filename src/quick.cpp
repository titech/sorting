template <class T>
void QS(int * array, Index start, Index end)
{
	T pivot = array[(start+end)/2];
	Index i = start, j = end;

    do  {
        while (array[i] < pivot) i++;
        while (array[j] > pivot) j--;
        if (i <= j) {
            if (i < j) std::swap(array[i], array[j]);
            i++;
            j--;
        }
    } while (i <= j);
    if (i < end)
        QS(array, i, end);
    if (start < j)
        QS(array, start,j);
}

template <class T>
void QuickSort(T * array,Index size)
{
	QS(array,0,size-1);
}
