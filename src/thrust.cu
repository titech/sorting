#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <cstdio>
#include <unistd.h>
#include <thrust/sort.h>
#include <thrust/device_vector.h>


typedef unsigned long long Index;

#include "sortutils.hpp"
#include "utils/timer.hpp"
void RunExperiment(Index cnt_keys)
{
    double secStart, secStop;
    std::cerr<<"creating array of random numbers \n";
    std::cerr<<"cnt keys = "<<static_cast<double>(cnt_keys)<<" \n";
    std::cerr<<"size data = "<<static_cast<double>(cnt_keys)*sizeof(datatype)/(1024*1024*1024)<<"G \n";
    datatype * array = new datatype [cnt_keys];
    int seed=time(NULL);
    FillRNDNums(array,array+cnt_keys,seed,256);
//    std::cerr << "Done in " << (clockStop - clockStart)/static_cast<double>(sysconf(_SC_CLK_TCK)) << " seconds\n\n";

    Print(array,cnt_keys);
    std::cerr<<"Doing thrust sort\n";
    secStart=Timer::GetSeconds();
    
    //thrust::device_vector<int> D(cnt_keys);
    thrust::device_vector<datatype> D(array, array+cnt_keys);
    thrust::sort(D.begin(), D.end());
    thrust::copy(D.begin(), D.end(), array);

    secStop=Timer::GetSeconds();
    double time_run=secStop-secStart;
    std::cerr << "Done in " <<  time_run<< " seconds\n";
    std::cerr << "keys per second " << cnt_keys/time_run << " \n";
    WriteStatsToFile("thrust_64b",cnt_keys,cnt_keys/time_run);
    Print(array,cnt_keys);
    delete [] array;
}

int main(int argc, char *argv[])
{
    //Index size=1LL*1000*1000*20;
    for (Index size=1ll*1000*100;size<20ll*1000*1000;size+=1ll*1000*50)
        RunExperiment(size);
    return 0;
}