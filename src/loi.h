/*  loi.h -- Low-overhead Instrumentation */
/*           This header provides access to a low-overhead profiler (LoSP) and access tracing library (KRD) */

#pragma once
#define LOI

#include <sys/time.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

// Some configuration require direct execution of syscalls. 
#include <sys/syscall.h>

// Some constants

// Max number of threads 
// Use NUMTHREADS to tune the number of threads for the specific platform 
//
#define MAXTHREADS 1024

// Maximum Number of kernels under consideration 
// You can change this if you know what you are doing
#define NUM_KERNELS  20    
#define NUM_PHASES   20
// 20 should be enough for most cases. I have not yet worked with more than 6 different kernels

// cycle counter type - used to store TSC
typedef uint64_t  ctimer_t;

/* I will just start with a definition of the interface */

// Structure for linking kernels and string identifiers
// Also provides info on phases
struct loi_kernel_info{
	int num_kernels;
	int num_phases;
	const char *kname[NUM_KERNELS]; // kernel names
	const char *phases[NUM_PHASES]; // phase names
	uint64_t phase_kernels[NUM_PHASES];  // each phase can only have up to 64 kernels inside (1 bit per kernel)
};

// each entry is only touched by a single core, so no locking is necessary
// aligned to 64 bytes to avoid false sharing
struct loi_kernel_stats{
  ctimer_t cycles[NUM_KERNELS] __attribute__ ((aligned (64)));
  uint64_t num[NUM_KERNELS] __attribute__((packed)); 
};

// same for application phases
struct loi_phase_stats{
   ctimer_t exectime[NUM_PHASES] __attribute__ ((aligned (64)));
};

// kernel statistics (per thread) and full phase statistics
extern struct loi_kernel_stats kernelstats [MAXTHREADS] __attribute__((aligned(64)));
extern struct loi_phase_stats  phasestats __attribute__((aligned(64)));

/* Initialization */

// Measure the speed of the local TSC and setup buffer space for KRD tracing

//extern "C"
//{
int loi_init(void);
//}

extern uint64_t TSCFREQ;

// some macros to convert from IDs into core ids and numa nodes
// TODO: check that the NUMA info starts at bit 20
#define CORE(id)   (id & 0xfff)
#define NUMA(id)   ((id >> 12) & 0xff)

// Macro to access kernel timers and counters
//   Uses: Kernel #id, kernel stats pointer, thread index

// If we are using RDTSCP method, then the TID will come as NUMA node + Core id
#ifdef RDTSCP_ID
// nc = numa/core
#define   Ktime(kid,nc)  (kernelstats[CORE(nc)].cycles[kid])
#define   Knum(kid,nc)   (kernelstats[CORE(nc)].num[kid])

#else

#define   Ktime(kid,tid)  (kernelstats[tid].cycles[kid])
#define   Knum(kid,tid)   (kernelstats[tid].num[kid])

#endif

// RDTSC and RDTSCP timer access

static inline ctimer_t __attribute__((unused)) rdtsc_clock_gettime(void)
{
      uint64_t clock_value;
      uint32_t low = 0;
      uint32_t high = 0;
      asm volatile ("rdtsc" : "=a" (low), "=d" (high));
      clock_value = ((uint64_t)high << 32) | (uint64_t)low;
      return (ctimer_t) clock_value;
}

// same functionality but using the serializing RDTSCP instruction
// in practice it does not make much difference in terms of performance. 
// However, RDTSCP also provides a Core ID, so we introduce the function below

// RDTSCP Cycle Timer
static inline ctimer_t __attribute__((unused)) rdtsc_clock_gettime_serial(void)
{
      uint64_t clock_value;
      uint32_t low = 0;
      uint32_t high = 0;
      uint32_t aux;
      asm volatile ("rdtscp" : "=a" (low), "=d" (high), "=c" (aux)); // we ignore the result in aux
      clock_value = ((uint64_t)high << 32) | (uint64_t)low;
      return (ctimer_t) clock_value;
}

// RDTSCP Cycle Timer and Core Id
static inline ctimer_t __attribute__((unused)) rdtsc_clock_gettime_serial_id(uint32_t  *id)
{
      uint64_t clock_value;
      uint32_t low = 0;
      uint32_t high = 0;
      if (!id) return 0;
      asm volatile ("rdtscp" : "=a" (low), "=d" (high), "=c" (*id)); 
      clock_value = ((uint64_t)high << 32) | (uint64_t)low;
      return (ctimer_t) clock_value;
}


/* Cycle Timers */

// by default use the RDTSC timer
// At least one test machine does not yet support the RDTSCP instruction 
#ifndef TIMER
 #define TIMER 1
#endif

#if TIMER == 1
#define   loi_gettime(id)    rdtsc_clock_gettime()            // RDTSC method
#elif TIMER == 2
#define   loi_gettime(id)    rdtsc_clock_gettime_serial()     // RDTSCP method
#else 
  #error "Invalid timer specification"
#endif


/* Core Identification */ 

// by default use sched_getcpu()
#ifndef THREADID
#define THREADID 5
#endif

#if THREADID == 1
#warning "For GETTID core id method, use NUMTHREADS = MAXTHREADSd"
#define threadid()  syscall(SYS_gettid)    // system call method, too much overhead
#elif THREADID == 2
#include <pthread.h>
#warning "For PTHREAD_SELF core id method, use NUMTHREADS = MAXTHREADSd"
#define threadid() pthread_self()          // works but is outside POSIX
#elif THREADID == 3 
#warning "Sigle-thread core id method, multithreaded results will be bogus"
#define threadid() ((0))   // this is just to avoid the API call when only 1 thread is used
#elif THREADID == 4
#define threadid() myth_get_worker_num()   // MassiveThreads method
#elif THREADID == 5
#ifdef _GNU_SOURCE
#include <sched.h>
#endif
#define threadid() sched_getcpu()   // Linux-specific, cross-architecture method
#else
#error "Invalid THREADID"
#endif

#if RDTSCP_ID
#if TIMER==1 
  #warning  "Using RDTSCP Core Id with RDTSC Timer. Is RDTSCP supported?"
#endif
#define  thread_index(coreid) (coreid)		 	// coreid contains the NUMA and Core id 
#define  loi_gettime_id(a)    rdtsc_clock_gettime_serial_id(a)        // RDTSCP method
#else
#define  thread_index(coreid) ((threadid() %MAXTHREADS) & 0x3ff)     // use THREADID, and get rid of bits more than 1024
#define  loi_gettime_id(a)    loi_gettime(a)                         // use TIMER
#endif

// translate from cycle count to time
// loi_init() must have been called
#define CTR(a) ((double) a/ (double) TSCFREQ)

// Generation of statistics
int loi_statistics(struct loi_kernel_info *, int total_threads); 

void loi_bench(void);

#ifdef LOI_TRACING
#include "krd.h"
#endif

void krd_bench();

// Finally here is an attempt to provide some concise macros
// The user needs to handle the scope by adding '{ }'

// for OMP profiling
#define PRAGMA(x)                       _Pragma( #x )
 
// Bare bones interface to reuse declared variables
#define profile_section()      ctimer_t __pstart, __pstop;

#define profile_section_start()      __pstart = loi_gettime()

#define profile_section_start_omp()     PRAGMA(omp master) \
                                        __pstart = loi_gettime();  \
                                        PRAGMA(omp barrier) 

#define profile_section_stop(phase)     __pstop = loi_gettime(); \
                                        phasestats.exectime[phase] = __pstop - __pstart; 

#define profile_section_stop_omp(phase)   PRAGMA(omp master) \
                                        { __pstop = loi_gettime(); \
                                        phasestats.exectime[phase] = __pstop - __pstart; }   \
                                        PRAGMA(omp barrier) 
// this is the integrated method

#define phase_profile_start()           ctimer_t __pstart, __pstop; \
                                        __pstart = loi_gettime(); 

// for OpenMP parallel sections only the master should record phase profiles
#define phase_profile_start_omp()       ctimer_t __pstart, __pstop; \
                                        PRAGMA(omp master) \
                                        __pstart = loi_gettime();  \
                                        PRAGMA(omp barrier) 

#define phase_profile_stop(phase)       __pstop = loi_gettime(); \
                                        phasestats.exectime[phase] = __pstop - __pstart; 

#define phase_profile_stop_omp(phase)   PRAGMA(omp master) \
                                        { __pstop = loi_gettime(); \
                                        phasestats.exectime[phase] = __pstop - __pstart; }   \
                                        PRAGMA(omp barrier) 

#define kernel_profile_start()          uint32_t __coreid __attribute__((unused)); int __ndx; \
                                        ctimer_t __kstart, __kstop __attribute__((unused)); \
                                        __kstart = loi_gettime();

#define kernel_profile_stop(kindex)     __kstop = loi_gettime_id(&__coreid); \
                                        __ndx = thread_index(__coreid); \
                                        Ktime(kindex,__ndx) += __kstop - __kstart; \
                                        Knum(kindex,__ndx) = Knum(kindex,__ndx) + 1; 

#define phase_print_runtime(phase)      printf("Running time for phase %d is %g s\n", phase, CTR(phasestats.exectime[phase]));

// Here are some macros to simplify tracing
// They rely on the same variables as the profiler and should be used consistently

#define  kernel_trace1(id, s)      			TTrace_add_and_incr(__ndx, ((uint64_t) id),  __kstart, s)
#define  kernel_trace2(id1, s1, id2, s2) 		TTrace_add_and_incr(__ndx, ((uint64_t) id1), __kstart, s1); \
							TTrace_add_and_incr(__ndx, ((uint64_t) id2), __kstart, s2)
#define  kernel_trace3(id1, s1, id2, s2, id3, s3)	TTrace_add_and_incr(__ndx, ((uint64_t) id1), __kstart, s1); \
							TTrace_add_and_incr(__ndx, ((uint64_t) id2), __kstart, s2); \
							TTrace_add_and_incr(__ndx, ((uint64_t) id3), __kstart, s3)

#define READ(b)    			(b)
#define WRITE(b)   			((-1)*(b))

// some definitions which are special for the sorting
enum kernels{
	HISTOGRAM = 0,
	REDUCTION,
	PREFIXSUM,
	OFFSETSPTP,
	MOVEKEYS,
    COPY,
	RECURSION
};

// 以上
//
