#include <iostream>
#include <cstring>
#include <omp.h>
#include <unistd.h>

#ifdef COMMON_CILKH
#include <common.cilkh>
#else
#define spawn {}
#define mk_task_group ;
#define create_task0(E) (E)
#define wait_tasks  ;
#endif

#include "string_sort_quick.hpp"
#include "utils/timer.hpp"

#define HARDCODE_TASK 16

extern Index num_threads;
Index cnt_tasks_total=0;
//Index cnt_active_tasks=0;
extern unsigned int cnt_tasks;

std::atomic<int> cnt_active_tasks;
std::atomic<int> cnt_tasks_waiting;

std::atomic<unsigned int> pos_taskprofile_buf;
Snapshot log_tasks[SIZE_TASKPROFILE_BUF];

inline void LogTaskIncrease()
{
    int local_pos_taskprofile=pos_taskprofile_buf++;
    log_tasks[local_pos_taskprofile].cnt_tasks=cnt_active_tasks++;
    log_tasks[local_pos_taskprofile].cnt_tasks_waiting=cnt_tasks_waiting;
    log_tasks[local_pos_taskprofile].timestamp=Timer::GetSeconds();
}
inline void LogTaskDecrease()
{
    int local_pos_taskprofile=pos_taskprofile_buf++;
    log_tasks[local_pos_taskprofile].cnt_tasks=cnt_active_tasks--;
    log_tasks[local_pos_taskprofile].cnt_tasks_waiting=cnt_tasks_waiting;
    log_tasks[local_pos_taskprofile].timestamp=Timer::GetSeconds();
}
inline void LogTaskIncreaseWait()
{
    int local_pos_taskprofile=pos_taskprofile_buf++;
    log_tasks[local_pos_taskprofile].cnt_tasks=cnt_active_tasks--;
    log_tasks[local_pos_taskprofile].cnt_tasks_waiting=cnt_tasks_waiting++;
    log_tasks[local_pos_taskprofile].timestamp=Timer::GetSeconds();
}
inline void LogTaskDecreaseWait()
{
    int local_pos_taskprofile=pos_taskprofile_buf++;
    log_tasks[local_pos_taskprofile].cnt_tasks=cnt_active_tasks++;
    log_tasks[local_pos_taskprofile].cnt_tasks_waiting=cnt_tasks_waiting--;
    log_tasks[local_pos_taskprofile].timestamp=Timer::GetSeconds();
}

//#define MY_WAIT LogTaskIncreaseWait(); wait_tasks; LogTaskDecreaseWait();
#define MY_WAIT wait_tasks;
//#define MY_WAIT #pragma omp wait


void saprint(const Index * sa, const byte * ref)
{
    for (Index i=0; i<10; i++)
    {
        for (Index j=0; j<10; j++)
            std::cerr<<static_cast<int>(ref[sa[i]+j])<<" ";
        std::cerr<<"\n";
    }
}

void SortStringRadixQuicksort( const Index start, const Index end, const Index depth, Index * sa, const byte * ref)
{
//std::cerr<<"--doing at depth "<<depth<<" from "<<start<<" to "<<end<<"\n";
    if (start+1>=end) return;
    Index lo=start, hi=end;
    Index i=start;
    Index tmp;
    byte pivot = ref[sa[i]+depth];
//std::cerr<<"pivot= "<<static_cast<int>(pivot)<<"\n";

    while (i<hi)
    {
        //  std::cerr<<"i= "<<i<<"\t c="<<static_cast<int>(ref[sa[i]+depth])<<"\n";
        if (ref[sa[i]+depth]<pivot)
        {
            tmp=sa[i];
            sa[i]=sa[lo];
            sa[lo]=tmp;
            // std::cerr<<"increasing low\n";
            lo++;
            i++;
        }
        else if (ref[sa[i]+depth]>pivot)
        {
            tmp=sa[i];
            sa[i]=sa[hi-1];
            sa[hi-1]=tmp;
            //std::cerr<<"decreasing high\n";
            hi--;
            //i++;
        }
        else i++;
    }
//saprint(sa, ref);

    /*    if (depth<6)
        {
          #pragma omp task
          if (start<lo) SortStringRadixQuicksort(start,lo,depth, sa,ref);
          #pragma omp task
          if (hi<end) SortStringRadixQuicksort(hi+1,end,depth,sa,ref);
          #pragma omp task
          if (depth<MAX_SORT_DEPTH)
            if (lo+1<hi)
          SortStringRadixQuicksort(lo,hi,depth+1,sa,ref);
          #pragma omp taskwait
        }
        else
     */
    {
        if (start+1<lo) SortStringRadixQuicksort(start,lo,depth, sa,ref);
        if (hi+1<end) SortStringRadixQuicksort(hi,end,depth,sa,ref);
        if (depth<MAX_SORT_DEPTH)
            if (lo+1<hi)
                SortStringRadixQuicksort(lo,hi,depth+1,sa,ref);
    }
}


void SortStringRadixQuicksortCounting( const Index start, const Index end, const Index depth, Index * sa, Index *sa_tmp, const byte * ref)
{
//std::cerr<<"--doing at depth "<<depth<<" from "<<start<<" to "<<end<<"\n";

// if (start+1>end) return;
//saprint(sa, ref);
   // #pragma omp atomic
   // cnt_tasks_total++;

    byte pivot = ref[sa[start]+depth]; //can randomize here
    // std::cerr<<"pivot= "<<static_cast<int>(pivot)<<"\n";
    Index cnt_less=0;
    Index cnt_eq=0;
    Index cnt_great=0;
    for (Index i=start; i<end; i++)
    {
        byte sym_current = ref[sa[i]+depth];
        if (sym_current<pivot) cnt_less++;
        else if (sym_current==pivot) cnt_eq++;
    }
    cnt_great=end-start-cnt_less-cnt_eq;
    Index posLess=start;
    Index posEq=posLess+cnt_less;
    Index posGreat=posEq+cnt_eq;
    //  std::cerr<<"counts ("<<cnt_less<<", "<<cnt_eq<<","<<cnt_great<<")\n";
    for (Index i=start; i<end; i++)
    {
        byte sym_current = ref[sa[i]+depth];
        if (sym_current<pivot)
            sa_tmp[posLess++]=sa[i];
        else if (sym_current==pivot)
            sa_tmp[posEq++]=sa[i];
        else
            sa_tmp[posGreat++]=sa[i];
    }
    for (Index i=start; i<end; i++)
        sa[i]=sa_tmp[i];
    if (depth>MAX_SORT_DEPTH) return;

    if (cnt_less>1) SortStringRadixQuicksortCounting(start, start+cnt_less, depth, sa, sa_tmp, ref);
    if (cnt_eq>1) SortStringRadixQuicksortCounting(start+cnt_less, start+cnt_less+cnt_eq, depth+1, sa, sa_tmp, ref);
    if (cnt_great>1) SortStringRadixQuicksortCounting(start+cnt_less+cnt_eq, start+cnt_less+cnt_eq+cnt_great, depth, sa, sa_tmp, ref);
}


struct CntsAtomic
{
  std::atomic<Index> less;
  std::atomic<Index> eq;
  std::atomic<Index> great;
};

inline void kernelCount(const Index start, const Index end, const Index depth, const Index * sa, const byte  *const ref, const byte pivot, Cnts *cnts_local, CntsAtomic * cnts_global)
{
    //LogTaskIncrease();
//	std::cerr << "kernelcount Start: " << start << " End: " << end << " cnts_local " << cnts_local << "\n";
    Cnts cnts;
    cnts.less=0;
    cnts.eq=0;
    cnts.great=0;
    for (Index i=start; i<end; i++)
    {
        byte sym_current = ref[sa[i]+depth];
        if (sym_current<pivot) cnts.less++;
        else if (sym_current==pivot) cnts.eq++;
    }
    cnts_local->less=cnts.less;
    cnts_local->eq=cnts.eq;
    cnts_local->great=end-start-cnts.less-cnts.eq;

    cnts_global->less+=cnts.less;
    cnts_global->eq+=cnts.eq;
    cnts_global->great+=end-start-cnts.less-cnts.eq;
    //LogTaskDecrease();
}

inline void kernelMove(const Index start, const Index end, const Index depth, const Index * sa, Index * sa_tmp, const byte  *const ref, const byte pivot, Cnts *cnts_local)
{
//LogTaskIncrease();
//	std::cerr << "kernelmove Start: " << start << " End: " << end << " cnts_local " << cnts_local << "\n";
    Cnts &offsets=*cnts_local;
    for (Index i=start; i<end; i++)
    {
        byte sym_current = ref[sa[i]+depth];
        if (sym_current<pivot)
            sa_tmp[offsets.less++]=sa[i];
        else if (sym_current==pivot)
            sa_tmp[offsets.eq++]=sa[i];
        else
            sa_tmp[offsets.great++]=sa[i];
    }
    //LogTaskDecrease();
}

inline void kernelCopyKeys(const Index start, const Index end, Index * sa, const Index * sa_temp)
{
    //LogTaskIncrease();
    for (Index i=start;i<end;i++)
        sa[i]=sa_temp[i];
    //LogTaskDecrease();
}

#include "partition.hpp"
#define THRESHOLD 10
// TODO: THRESHOLD should be based on task size, but specifying depth simplifies the code


void countRecursion(const Index start, const Index end, const Index depth, const Index * sa, const byte  *const ref, const byte pivot, Cnts *cnts_local, CntsAtomic * cnts_global, int i, int recDepth)
{

	if(recDepth == THRESHOLD)
		return kernelCount(start, end, depth, sa, ref, pivot, cnts_local+i, cnts_global);

#if COMMON_CILKH
	mk_task_group;
	create_task0(spawn countRecursion(start, start+((end-start)/2), depth, sa, ref, pivot, cnts_local, cnts_global, i*2, recDepth+1));
	countRecursion(start+((end-start)/2), end, depth, sa, ref, pivot, cnts_local, cnts_global, 1+(i*2), recDepth+1);
	wait_tasks;
#else
	countRecursion(start, start+((end-start)/2), depth, sa, ref, pivot, cnts_local, cnts_global, i*2, recDepth+1);
	countRecursion(start+((end-start)/2), end, depth, sa, ref, pivot, cnts_local, cnts_global, 1+(i*2), recDepth+1);
#endif
}

void moveRecursion(const Index start, const Index end, const Index depth, const Index * sa, Index * sa_tmp, const byte  *const ref, const byte pivot, Cnts *cnts_local, int i, int recDepth)
{
	if(recDepth == THRESHOLD)
		return kernelMove(start, end, depth, sa, sa_tmp, ref, pivot, cnts_local+i);

#if COMMON_CILKH
	mk_task_group;
	create_task0(spawn moveRecursion(start, start+((end-start)/2), depth, sa, sa_tmp, ref, pivot, cnts_local, i*2, recDepth+1));
	moveRecursion(start+((end-start)/2), end, depth, sa, sa_tmp, ref, pivot, cnts_local, 1+(i*2), recDepth+1);
	wait_tasks;
#else
	moveRecursion(start, start+((end-start)/2), depth, sa, sa_tmp, ref, pivot, cnts_local, i*2, recDepth+1);
	moveRecursion(start+((end-start)/2), end, depth, sa, sa_tmp, ref, pivot, cnts_local, 1+(i*2), recDepth+1);
#endif
}

void SortStringRadixQuicksortCountingTasks(const Index start, const Index end, const Index depth, Index * sa, Index *sa_tmp, const byte  * const ref)
{
    if (start+1>=end) return;

    byte pivot = ref[sa[start]+depth]; //can randomize here // should be after treshhold check
//    Calc3WayHistogram(start, end, depth,sa, ref, pivot);
//    return;

    //LogTaskIncrease();
    Index treshold=10000;
    if ((end-start)<treshold)
    {
        SortStringRadixQuicksort(start, end, depth, sa,  ref);
      //  LogTaskDecrease();
        return;
    }

//std::cerr<<"--doing at depth "<<depth<<" from "<<start<<" to "<<end<<"\n";
    //#pragma omp atomic
    //cnt_active_tasks++;
    //std::cerr<<"cnt active tasks = " <<cnt_active_tasks<<"\n";


    //std::cerr<<"pivot= "<<static_cast<int>(pivot)<<"\n";

    CntsAtomic a_cnts;
    a_cnts.less=0;
    a_cnts.eq=0;
    a_cnts.great=0;
    CntsAtomic * p_cnts=&a_cnts;
  //  const unsigned int cnt_tasks=HARDCODE_TASK;
    double cnt_lines_per_task=(double(end-start))/cnt_tasks;

    Cnts cnts;
    cnts.less=0;
    cnts.eq=0;
    cnts.great=0;

    mk_task_group;
  //  if (cnt_active_tasks<num_threads*2)
    if (depth<2)
    {
        //Cnts * arr_cnts = new Cnts[cnt_tasks];
        Cnts * arr_cnts = new Cnts[1<<(THRESHOLD+3)];
        //Cnts arr_cnts[HARDCODE_TASK];
        // memset(arr_cnts,0,cnt_tasks*sizeof(Cnts));
       // #pragma omp atomic
       // cnt_tasks_total+=cnt_tasks*2;

//        for (unsigned int i=0; i<cnt_tasks; i++)
//        {
//            Index pos_task_begin= start+(cnt_lines_per_task*i);
//            Index pos_task_end  = start+(cnt_lines_per_task*(i+1));
//            //#pragma omp task shared(arr_cnts)
//            create_task0(spawn kernelCount(pos_task_begin, pos_task_end, depth, sa, ref, pivot, arr_cnts+i,p_cnts));
//           // kernelCount(pos_task_begin, pos_task_end, depth, sa, ref, pivot, arr_cnts+i,p_cnts);
//        }
        countRecursion(start, end, depth, sa, ref, pivot, arr_cnts, p_cnts, 0, 0);

        MY_WAIT;
        cnts.less=a_cnts.less;
        cnts.eq=a_cnts.eq;
        cnts.great=a_cnts.great;
//	std::cerr<<"cnt_less  = "<<cnts.less<<"\n";
//	std::cerr<<"cnt_eq    = "<<cnts.eq<<"\n";
//	std::cerr<<"cnt_great = "<<cnts.great<<"\n";

        Cnts offsets;
        offsets.less=start;
        offsets.eq=offsets.less+cnts.less;
        offsets.great=offsets.eq+cnts.eq;
        Cnts I;
        I.less=0;
        I.eq=0;
        I.great=0;

        //std::cerr<<"counts ("<<cnts.less<<", "<<cnts.eq<<","<<cnts.great<<")\n";
        //std::cerr<<"offsets ("<<offsets.less<<", "<<offsets.eq<<","<<offsets.great<<")\n";

        //for (unsigned int i=0; i<cnt_tasks; i++)
        for (unsigned int i=0; i<(1<<THRESHOLD); i++)
        {
            Cnts temp;
            temp.less=arr_cnts[i].less;
            temp.eq=arr_cnts[i].eq;
            temp.great=arr_cnts[i].great;
            arr_cnts[i].less=I.less+offsets.less;
            arr_cnts[i].eq=I.eq+offsets.eq;
            arr_cnts[i].great=I.great+offsets.great;
            I.less+=temp.less;
            I.eq+=temp.eq;
            I.great+=temp.great;
        }

//return;
//        for (unsigned int i=0; i<cnt_tasks; i++)
//        {
//           Index pos_task_begin= start+(cnt_lines_per_task*i);
//           Index pos_task_end  = start+(cnt_lines_per_task*(i+1));
//           // #pragma omp task
//           create_task0(spawn kernelMove(pos_task_begin, pos_task_end, depth, sa, sa_tmp, ref, pivot, arr_cnts+i));
//          // kernelMove(pos_task_begin, pos_task_end, depth, sa, sa_tmp, ref, pivot, arr_cnts+i);
//        }
//    //    #pragma omp taskwait
	moveRecursion(start, end, depth, sa, sa_tmp, ref, pivot, arr_cnts, 0, 0);
        MY_WAIT;



        delete[] arr_cnts;
    }
    else //count and moce sequentially
    {
        for (Index i=start; i<end; i++)
        {
            byte sym_current = ref[sa[i]+depth];
            if (sym_current<pivot) cnts.less++;
            else if (sym_current==pivot) cnts.eq++;
        }
        cnts.great=end-start-cnts.less-cnts.eq;
        Index posLess=start;
        Index posEq=posLess+cnts.less;
        Index posGreat=posEq+cnts.eq;
        for (Index i=start; i<end; i++)
        {
            byte sym_current = ref[sa[i]+depth];
            if (sym_current<pivot)
                sa_tmp[posLess++]=sa[i];
            else if (sym_current==pivot)
                sa_tmp[posEq++]=sa[i];
            else
                sa_tmp[posGreat++]=sa[i];
        }
    }

    //if (cnt_active_tasks<num_threads*2)
    if (depth<2)
    {
    //mk_task_group;
        for (unsigned int i=0; i<cnt_tasks; i++)
        {
            Index pos_task_begin= start+(cnt_lines_per_task*i);
            Index pos_task_end  = start+(cnt_lines_per_task*(i+1));
            //#pragma omp task
#if COMMON_CILKH
            create_task0(spawn kernelCopyKeys(pos_task_begin,pos_task_end,sa,sa_tmp));
#else
            kernelCopyKeys(pos_task_begin,pos_task_end,sa,sa_tmp);
#endif
        }
#if COMMON_CILKH
        MY_WAIT;
#endif

    }
    else
    {
        for (Index i=start; i<end; i++)
           sa[i]=sa_tmp[i];
    }
    return;
    if (depth>MAX_SORT_DEPTH) return;


   // LogTaskIncreaseWait();
#if COMMON_CILKH
    create_task0(spawn  SortStringRadixQuicksortCountingTasks(start, start+cnts.less, depth, sa, sa_tmp, ref));
    create_task0(spawn  SortStringRadixQuicksortCountingTasks(start+cnts.less, start+cnts.less+cnts.eq, depth+1, sa, sa_tmp, ref));
    create_task0(spawn  SortStringRadixQuicksortCountingTasks(start+cnts.less+cnts.eq,start+cnts.less+cnts.eq+cnts.great, depth, sa, sa_tmp, ref));
#else

    SortStringRadixQuicksortCountingTasks(start, start+cnts.less, depth, sa, sa_tmp, ref);
    SortStringRadixQuicksortCountingTasks(start+cnts.less, start+cnts.less+cnts.eq, depth+1, sa, sa_tmp, ref);
    SortStringRadixQuicksortCountingTasks(start+cnts.less+cnts.eq,start+cnts.less+cnts.eq+cnts.great, depth, sa, sa_tmp, ref);
#endif
  //  LogTaskDecreaseWait();


//#pragma omp task
//    SortStringRadixQuicksortCountingTasks(start, start+cnts.less, depth, sa, sa_tmp, ref);
//#pragma omp task
    //SortStringRadixQuicksortCountingTasks(start+cnts.less, start+cnts.less+cnts.eq, depth+1, sa, sa_tmp, ref);
//#pragma omp task
    //SortStringRadixQuicksortCountingTasks(start+cnts.less+cnts.eq,start+cnts.less+cnts.eq+cnts.great, depth, sa, sa_tmp, ref);
    //#pragma omp taskwait
/*
    #pragma omp atomic
    cnt_active_tasks--;

    */
    //LogTaskDecrease();
#ifdef COMMON_CILKH
    wait_tasks;
#endif
}

void PrintSpace(Index c)
{
    for (Index i=0; i<c; i++) std::cerr<<" ";
}

