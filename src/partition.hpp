void Calc3WayHistogram(const Index start, const Index end, const Index depth, Index * sa, const byte  * const ref, const byte pivot)
{
	Index cnt_less=0;
	Index cnt_eq=0;
	Index cnt_great=0;
#pragma omp parallel for reduction(+:cnt_less) reduction(+:cnt_eq) reduction(+:cnt_great)
	for (Index i=start;i<end;i++)
	{
		if (ref[sa[i]+depth]<pivot) cnt_less++;
		else 
		{
			if (ref[sa[i]+depth]==pivot) cnt_eq++;
			else cnt_great++;
		}
	}
	std::cerr<<"cnt_less  = "<<cnt_less<<"\n";
	std::cerr<<"cnt_eq    = "<<cnt_eq<<"\n";
	std::cerr<<"cnt_great = "<<cnt_great<<"\n";
}
