#include <iostream>
#include <cstring>
#include <omp.h>

#include "string_sort_radix.hpp"
#include "string_sort_quick.hpp"
#include "sortutils.hpp"
#include "string_array.hpp"

#ifdef DO_LOI
#include "loi.h"
#endif

extern unsigned int num_threads;


    #ifdef MIC
void __attribute__((target(mic))) SortStringsCountRecursive(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef)
    #else
void SortStringsCountRecursive(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef)
    #endif
{
    if (scope.start>=scope.end) return;
  //  WriteStatsToFile("buckets", scope.depth, scope.end-scope.start);
    Index count_letters[LENGTH_ALPHABET]= {0};
    Index count_offsets[LENGTH_ALPHABET]= {0};

    for (Index pos=scope.start; pos<scope.end; pos++)
    {
        byte currentSymbol=ref[(sa[pos]+scope.depth)%sizeRef];
        count_letters[currentSymbol]++;
    }

    for (int i=1; i<LENGTH_ALPHABET; i++)
        count_offsets[i]=count_offsets[i-1]+count_letters[i-1];

    for (Index idLine=scope.start; idLine<scope.end; idLine++)
    {
        byte currentSymbol=ref[(sa[idLine]+scope.depth)%sizeRef];

        sa2[scope.start+(count_offsets[currentSymbol])++]=sa[idLine];
    }
    for (Index idLine=scope.start; idLine<scope.end; idLine++)
        sa[idLine]=sa2[idLine];
    if (scope.depth<MAX_SORT_DEPTH-1)
    {
        for (int i=0; i<LENGTH_ALPHABET; i++)
        {
            if(count_letters[i]>1)
            {
                SortStringsCountRecursive(SortScope((i==0?0:count_offsets[i-1])+scope.start,count_offsets[i]+scope.start,scope.depth+1),sa,sa2, ref, sizeRef);
            }
        }
    }
    else  //here we sort "tails"  - small unsorted buckets but I'm not sure it make sence to sort like this
    {
//      for (int i=0; i<LENGTH_ALPHABET; i++)
//          if(count_letters[i]>1)
                //std::sort(sa+(i==0?0:count_offsets[i-1])+scope.start,sa+count_offsets[i]+scope.start,ComparatorStrings(ref,scope.depth,sizeRef));
    }
}

void SortStringsCountRecursiveOMP(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef)
{
    //if (scope.depth==8)
    if (scope.start>=scope.end) return;
    Index count_letters[LENGTH_ALPHABET]= {0};
    Index count_offsets[LENGTH_ALPHABET]= {0};
    Index * pos_start=sa2+scope.start;

    for (Index pos=scope.start; pos<scope.end; pos++)
    {
        byte currentSymbol=ref[(sa[pos]+scope.depth)];
        count_letters[currentSymbol]++;
    }
    for (int i=1; i<LENGTH_ALPHABET; i++)
        count_offsets[i]=count_offsets[i-1]+count_letters[i-1];

    for (Index idLine=scope.start; idLine<scope.end; idLine++)
    {
        byte currentSymbol=ref[(sa[idLine]+scope.depth)];
        sa2[idLine]=1;
        *(pos_start+(count_offsets[currentSymbol])++)=sa[idLine];
        sa2[scope.start+(count_offsets[currentSymbol])++]=sa[idLine];
    }

    for (Index idLine=scope.start; idLine<scope.end; idLine++)
        sa[idLine]=sa2[idLine];

//return;

    if (scope.depth<MAX_SORT_DEPTH-2)
    {
        for (int i=0; i<LENGTH_ALPHABET; i++)
        {
            if(count_letters[i]>1)
            {
                SortStringsCountRecursive(SortScope((i==0?0:count_offsets[i-1])+scope.start,count_offsets[i]+scope.start,scope.depth+1),sa,sa2, ref, sizeRef);
            }
        }
    }
    else  //here we sort "tails"  - small unsorted buckets but I'm not sure it make sence to sort like this
    {
//      for (int i=0; i<LENGTH_ALPHABET; i++)
//          if(count_letters[i]>1)
                //std::sort(sa+(i==0?0:count_offsets[i-1])+scope.start,sa+count_offsets[i]+scope.start,ComparatorStrings(ref,scope.depth,sizeRef));
    }
}

void SortStringsCountAutomataOMP(const Index start, const Index end ,const Index depth, Index * sa,Index * saTemp, const byte * ref)
{
 // std::cerr<<"sorting from "<<start<<" to "<<end<<" at depth "<<depth<<"\n";
 //  Timer timer1;
 //  timer1.Notch();

    const Index cntBuckets = GetNumBuckets(LENGTH_ALPHABET,LEN_WORD);
    Index * counter_words = new Index[cntBuckets+1];
    memset(counter_words, 0, sizeof(Index)*(cntBuckets+1));

#ifdef DO_LOI
     profile_section();
#endif
    Index * counter_offsets = new Index[cntBuckets+1];
//    memset(counter_offsets, 0, sizeof(Index)*(cntBuckets+1));
    counter_offsets[0]=0;
    Index ** counters_local =  new  Index * [num_threads];
    #pragma omp parallel  shared(counter_words,counter_offsets)
    {
#ifdef DO_LOI
     profile_section_start_omp();
#endif
#ifdef _OPENMP
        int id_thread= omp_get_thread_num();
#else
        int id_thread=0;
#endif
        Index * buf_local= new Index[cntBuckets+1];
        counters_local[id_thread]=buf_local;
        for (unsigned long i=0; i<cntBuckets+1; i++)
            buf_local[i]=0;
       // #pragma omp barrier

        // ###################Histogram ?-----
        #pragma omp for
        for (Index idLine=start; idLine<end; idLine++)
        {
#ifdef DO_LOI
     kernel_profile_start();
#endif
            Index state=0;
            #pragma vector aligned
            for (Index posSymbol=sa[idLine]+depth;posSymbol<sa[idLine]+depth+LEN_WORD;posSymbol++)
                state=(state*LENGTH_ALPHABET+ref[posSymbol]);
            buf_local[state]++;
           // counter_words[state]++;
#ifdef DO_LOI
     kernel_profile_stop(HISTOGRAM);
#endif
        }

#ifdef DO_LOI
     profile_section_stop_omp(HISTOGRAM);
     profile_section_start_omp();
#endif

//     std::cout << " counting phase over " << std::endl;
        //################reductiuon------
        #pragma omp for
        for (unsigned long i=0; i<cntBuckets; i++)
        {
            for (unsigned int j=0;j<num_threads;j++)
                counter_words[i]+=counters_local[j][i];
        }

#ifdef DO_LOI
     profile_section_stop_omp(REDUCTION);
     profile_section_start_omp();
#endif
 //    std::cout << " reduction phase over " << std::endl;
        //######### prefix sum ########### (can be joined w reduction)
        #pragma omp single
        for (unsigned int i=1; i<cntBuckets; i++)
        {
            counter_offsets[i]=counter_offsets[i-1]+counter_words[i-1];
        }

#ifdef DO_LOI
     profile_section_stop_omp(PREFIXSUM);
     profile_section_start_omp();
#endif
   //  std::cout << " offsets phase over " << std::endl;
        // ######################offseting per-thread pointers
        #pragma omp for
        for (unsigned long i=0; i<cntBuckets; i++)
        {
            for (unsigned int j=1;j<num_threads;j++)
                counters_local[j][i]+=counters_local[j-1][i];
        }
#ifdef DO_LOI
     profile_section_stop_omp(OFFSETSPTP);
     profile_section_start_omp();
#endif

//     std::cout << " I-do-not-know-what phase over " << std::endl;
        #pragma omp for  // ############3 MOVE KEYS
        for (Index idLine=start; idLine<end; idLine++)
        {
#ifdef DO_LOI
     kernel_profile_start();
#endif
           Index state=0;
            for (Index posSymbol=sa[idLine]+depth;posSymbol<sa[idLine]+depth+LEN_WORD;posSymbol++)
                state=(state*LENGTH_ALPHABET+ref[posSymbol]);
            saTemp[(counter_offsets[state])-1+buf_local[state]--]=sa[idLine];
#ifdef DO_LOI
     kernel_profile_stop(MOVEKEYS);
#endif
        }
        delete [] buf_local;

#ifdef DO_LOI
     profile_section_stop_omp(MOVEKEYS);
     profile_section_start_omp();
#endif

  //   std::cout << " another phase over " << std::endl;
    #pragma omp for //simd //copy keys
    for (Index idLine=start; idLine<end; idLine++)
        sa[idLine]=saTemp[idLine];  //do some vector copies here  or memcpy
  //   std::cout << " copying phase over " << std::endl;
//}
//return;

#ifdef DO_LOI
	profile_section_stop_omp(COPY);
	profile_section_start_omp();
#endif
//#pragma omp parallel
//{
//  #pragma omp single
//    {
    #pragma omp for
        for (unsigned int i=0; i<cntBuckets; i++)
        {
        if (counter_words[i]>1)
        {
    //    std::cerr<<"unsoerted bucket\n";
            #pragma omp task untied
                {

#ifdef DO_LOI
     kernel_profile_start();
#endif
            SortStringRadixQuicksort(counter_offsets[i],counter_offsets[i]+counter_words[i],LEN_WORD, sa,ref);
#ifdef DO_LOI
     kernel_profile_stop(RECURSION);
#endif
                }
                //SortStringsCountRecursive(SortScope(counter_offsets[i],counter_offsets[i]+counter_words[i], depth+LEN_WORD),sa,saTemp,ref ,refsize);
        }
        }
        #pragma omp taskwait
    //}
#ifdef DO_LOI
     profile_section_stop_omp(RECURSION);
#endif
   }
return;


    //    timer1.Notch();
  //  my_stats.Accumulate("stage1",timer1.GetLastIntervalSeconds());
    //std::cerr<<"launching tasts\n";
    //timer1.Notch();

//    Index cnt_sorted=0;
 //   for (unsigned int i=0; i<cntBuckets; i++)
 //   {
  //      std::cerr<<counter_words[i]<<" ";
  //      if (counter_words[i]<2)
  //          cnt_sorted++;
   // }
   // std::cerr<<"count sorted buckets = " <<cnt_sorted<<"\n";
   delete[] counters_local;
   delete[] counter_offsets;
   delete[] counter_words;
return;
/*
    if (depth>MAX_SORT_DEPTH-1) return;
    if ((end-start)>10000)
    {
        for (unsigned int i=0; i<cntBuckets; i++)
        {
            if (counter_words[i]>1)
            {
               SortStringsCountAutomataOMP(counter_offsets[i],counter_offsets[i]+counter_words[i], depth+LEN_WORD,sa,saTemp,ref ,refsize);
            }
        }
    }
else
    {
        #pragma omp parallel
        {
        #pragma omp master
        for (unsigned int i=0; i<cntBuckets; i++)
        {
        if (counter_words[i]>1)
        {
                #pragma omp task
                // SortStringRadixQuicksort(counter_offsets[i],counter_offsets[i]+counter_words[i]-1,LEN_WORD, sa,ref);
                SortStringsCountRecursive(SortScope(counter_offsets[i],counter_offsets[i]+counter_words[i], depth+LEN_WORD),sa,saTemp,ref ,refsize);
        }
        }
        #pragma omp taskwait
        }
    }
    */
  //  timer1.Notch();
    //my_stats.Accumulate("stage2",timer1.GetLastIntervalSeconds());

}

/*
void SortStringsCountAutomataOffload(const Index start, const Index end, const Index depth, Index * sa,Index * saTemp, byte * ref,const Index refsize)
{
//    const Index cntLines = end-start;
  //  #pragma offload target(mic:MIC_DEV) in(start) in(end) in(depth) inout(sa:length(cntLines)  alloc_if(0) free_if(0)) inout(saTemp:length(cntLines)  alloc_if(0) free_if(0)) in(ref:length(refsize) alloc_if(0) free_if(0)) in(cntBuckets)
//    SortStringsCountAutomataOMP(start,end,depth, sa,saTemp,ref,refsize);
    return;
}
*/

/*
    #ifdef MIC
void __attribute__((target(mic))) SortStringsCountRecursiveTasks(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef)
    #else
void SortStringsCountRecursiveTasks(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef)
    #endif
{
    if (scope.start>=scope.end) return;
    Index count_letters[LENGTH_ALPHABET]= {0};
    Index count_offsets[LENGTH_ALPHABET]= {0};

    for (Index pos=scope.start; pos<scope.end; pos++)
    {
        byte currentSymbol=ref[(sa[pos]+scope.depth)%sizeRef];
        count_letters[currentSymbol]++;
    }
    for (int i=1; i<LENGTH_ALPHABET; i++)
        count_offsets[i]=count_offsets[i-1]+count_letters[i-1];
    for (Index idLine=scope.start; idLine<scope.end; idLine++)
    {
        byte currentSymbol=ref[(sa[idLine]+scope.depth)%sizeRef];
        sa2[scope.start+(count_offsets[currentSymbol])++]=sa[idLine];
    }
    for (Index idLine=scope.start; idLine<scope.end; idLine++)
        sa[idLine]=sa2[idLine];
    if (scope.depth<MAX_SORT_DEPTH-1)
    {
        for (int i=0; i<LENGTH_ALPHABET; i++)
        {
            if(count_letters[i]>1)
            {
            	if (scope.depth<6)
            	{
            		#pragma omp task
                	SortStringsCountRecursive(SortScope((i==0?0:count_offsets[i-1])+scope.start,count_offsets[i]+scope.start,scope.depth+1),sa,sa2, ref, sizeRef);
                }
                else
           	    	SortStringsCountRecursive(SortScope((i==0?0:count_offsets[i-1])+scope.start,count_offsets[i]+scope.start,scope.depth+1),sa,sa2, ref, sizeRef);
                }
        }
        #pragma omp taskwait
    }
}
*/
