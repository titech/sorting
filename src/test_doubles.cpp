#include <iostream>
#include <algorithm>
#include <cstdio>

#define LEN_ALPHABETH 256;

#include "sortutils.hpp"

#include "selection.cpp"
#include "quick_my.cpp"


void RunExperiments(Index size)
{
    double * array = new double [size];
   //   RunExperiment(SelectionSort<datatype>,array,size,"selection",seed);
//    RunExperiment(InsertionSort<datatype>,array,size,"insertion",seed);
    int seed=1;
    RunExperiment(QuickSort<double>,array,size,"quicksort_for1",seed);
  //  std::cerr<<"size = "<<static_cast<double>(size	)/(1024*1024*1024)<<"G \n";
  //  FillRNDNums(array,array+cnt_keys,seed,65536);
  
    std::cerr<<"\n";
    delete [] array;
}

int main(int argc, char *argv[])
{
    Index size=1LL*10000;
  	//for (size=1ll*1000*1000;size<40ll*1000*1000;size+=1ll*1000*1000)
        RunExperiments(size);
    return 0;
}