#ifndef H_STRING_ARRAY
#define H_STRING_ARRAY

#include "utils/types.hpp"

#ifdef MIC
Index __attribute__((target(mic))) GetNumBuckets(const Index size_alphabet,const Index len_word);
#else
Index GetNumBuckets(const Index size_alphabet,const Index len_word);
#endif

struct StringArray
{
    Index count;
    Index * offsets;
    byte * buf_data;
    StringArray():count(0),offsets(NULL),buf_data(NULL){}
    void PrintString(Index idx);
    void PrintHead();
    StringArray Trim();
};  


class ComparatorStringArray
{
	public:
	const StringArray & sa;
	ComparatorStringArray(const StringArray & _sa):sa(_sa){};
	bool operator() (Index i,Index j) ;
};


StringArray GenerateRandomStrings(Index cntKeys, Index lenKey);

#endif