#define NOMINMAX

#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <algorithm>
#include <cmath>
#include <cstring> 
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <omp.h>

#include "utils/mycuda.hpp"
#include "sortutils.hpp"
#include "string_array_gpu.hpp"
#include "kernels_bucket.cu"
#include "string_sort_quick.hpp"
#include "string_sort_radix.hpp"

extern StatWriter my_stats;
unsigned int num_threads;
typedef byte datatype;

double gtime;

void SortCudaFake(datatype * array,Index size)
{
  datatype * gpuArray = NULL;
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start, 0);
  gpuErrchk( cudaMalloc(&gpuArray, size*sizeof(datatype)) );
  gpuErrchk( cudaMemcpy(gpuArray,array, size*sizeof(datatype), cudaMemcpyHostToDevice) );
  gpuErrchk( cudaMemcpy(array, gpuArray, size*sizeof(datatype), cudaMemcpyDeviceToHost) );
  gpuErrchk( cudaFree(gpuArray) );
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
}

void TestCudaResult()
{
    cudaError_t er=cudaGetLastError();
    if ( cudaSuccess != er )
    std::cerr<<"ERROR IN KERNEL LAUNCH: "<<cudaGetErrorString (er) <<std::endl;
}

void CudaPrintFreeMem()
{
  size_t size_free;
  size_t size_total;
  cudaMemGetInfo(&size_free,&size_total);
  std::cerr<<"gpu mem total = "<<size_total<<"\t mem free = "<<size_free<<"\n";
}

void RunKrnlSort(GPUStringArray & gpu_sa,StringArray & cpu_sa)
{
      Timer timer1;
  //CudaPrintFreeMem();
	int cntBlocks=32;
	int cntThreadsPerBlock=128;
	int cntThreadsTotal=cntThreadsPerBlock*cntBlocks;
  dim3 dimBlock(cntThreadsPerBlock, 1);
  dim3 dimGrid(cntBlocks, 1);
  Index *gpuBucketsCnts=NULL;
  Index *gpuBucketsOffsets=NULL;
  Index cnt_buckets=GetNumBuckets(LENGTH_ALPHABET,LEN_WORD);
  Index sizeMemBuckets= (cnt_buckets+1)*sizeof(Index);
  gpuErrchk(cudaMalloc((void**) &gpuBucketsCnts,sizeMemBuckets));
  gpuErrchk(cudaMalloc((void**) &gpuBucketsOffsets, sizeMemBuckets));
  cudaEvent_t start,stage1,stage2,stage3,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stage1);
  cudaEventCreate(&stage2);
  cudaEventCreate(&stage3);
  cudaEventCreate(&stop);
  cudaEventRecord(start, 0);
  Index * gpuTempSA=NULL;
     timer1.Notch();
  gpuErrchk(cudaMalloc((void**) &gpuTempSA, gpu_sa.count*sizeof(Index)));
  gpuErrchk(cudaMemset  ( gpuBucketsCnts,0  ,sizeMemBuckets));
  
  krnlCountBucketsString<<<dimGrid,dimBlock>>>(gpuBucketsCnts, gpu_sa.offsets, gpu_sa.buf_data, gpu_sa.count);
//  gpuErrchk( cudaEventRecord(stage1, 0) );
//  gpuErrchk( cudaEventSynchronize(stage1) );
 //   timer1.Notch();
   // my_stats.Accumulate("3count",timer1.GetLastIntervalSeconds());
/*  Index * cpu_offsets=new Index[gpu_sa.count];
  gpuErrchk (cudaMemcpy(cpu_offsets, gpuBucketsCnts,cnt_buckets * sizeof(Index), cudaMemcpyDeviceToHost));
  for (Index i=0;i<cnt_buckets;i++)
    std::cerr<<cpu_offsets[i]<<"\n";
  delete[] cpu_offsets;
*/
  //  timer1.Notch();
  krnlCalcOffsets<<<1,1>>>(gpuBucketsCnts,gpuBucketsOffsets,cnt_buckets);
  //gpuErrchk( cudaEventRecord(stage2, 0) );
  //gpuErrchk( cudaEventSynchronize(stage2) );
   // timer1.Notch();
   //my_stats.Accumulate("4offsets",timer1.GetLastIntervalSeconds());

  //TestCudaResult();
//  std::cerr<<"gpu_count= "<<gpu_sa.count<<"\n";
    timer1.Notch();
 krnlBuildPointersByBuckets<<<dimGrid,dimBlock>>>(gpuBucketsOffsets,gpu_sa.offsets,gpuTempSA,gpu_sa.buf_data,gpu_sa.count);
 
//my_stats.Accumulate("time_gpu",gtime);
//do second stage----------------------------
  //rebuild offsets

    //byte  * gpuBordersBuckets=NULL;
    //gpuErrchk(cudaMalloc((void**) &gpuBordersBuckets, gpu_sa.count));
    krnlCalcOffsets<<<1,1>>>(gpuBucketsCnts,gpuBucketsOffsets,cnt_buckets);
    krnlSortTasks<<<dimGrid,dimBlock>>>(LEN_WORD,gpuBucketsOffsets,cnt_buckets,gpuTempSA, gpu_sa.buf_data);
//    unsigned long cntBlocksReduceBuckets=(cnt_buckets+cntThreadsPerBlock-1)/cntThreadsPerBlock;
//    krnlSortRecursive<<<cntBlocksReduceBuckets,cntThreadsPerBlock>>>(LEN_WORD,gpuBucketsOffsets,cnt_buckets,gpuTempSA,gpu_sa.offsets,gpu_sa.buf_data);
  gpuErrchk( cudaEventRecord(stage3, 0) );
  gpuErrchk( cudaEventSynchronize(stage3) );
  float time_gpu=0;
  cudaEventElapsedTime   (&time_gpu,start,stage3);   
  //  timer1.Notch();
  // my_stats.Accumulate("5move",timer1.GetLastIntervalSeconds());
  gtime=time_gpu/1000;
    
    
    //Index *gpuBuckets2StageCnts=NULL;
    //Index *gpuBuckets2StageOffs=NULL;
    //cntBlocks=128;
  //  gpuErrchk(cudaMalloc((void**) &gpuBuckets2StageCnts,sizeMemBuckets*cntBlocks));
  //  gpuErrchk(cudaMalloc((void**) &gpuBuckets2StageOffs,sizeMemBuckets*cntBlocks));
  //  gpuErrchk(cudaMemset   ( gpuBuckets2StageCnts,0  ,(cnt_buckets+1)*sizeof(Index)*cntBlocks));
  //  kenlSortPref2Stage<<<cntBlocks,cntThreadsPerBlock>>>(LEN_WORD,gpuBucketsOffsets,cnt_buckets,gpuBuckets2StageCnts,gpuBuckets2StageOffs,gpuTempSA,gpu_sa.offsets,gpuBordersBuckets,gpu_sa.buf_data );
  //  gpuErrchk(cudaFree(gpuBuckets2StageCnts));
  //  gpuErrchk(cudaFree(gpuBuckets2StageOffs));
  //  gpuErrchk(cudaFree(gpuBordersBuckets));

  //launch  second stage sort per bucket
    timer1.Notch();

  TestCudaResult();
  Index * gpuTempSA2=gpu_sa.offsets;
  gpu_sa.offsets=gpuTempSA;
  gpu_sa.CopyFromGPU(cpu_sa);
  gpuErrchk(cudaFree(gpuTempSA2));

   timer1.Notch();
   my_stats.Accumulate("6move_results",timer1.GetLastIntervalSeconds());

  //-------------------- third stage
 //  return;
   /*
  Index * cpuBuckets = new Index[cnt_buckets+1];
  gpuErrchk (cudaMemcpy(cpuBuckets, gpuBucketsOffsets, (cnt_buckets+1)* sizeof(Index), cudaMemcpyDeviceToHost));
  //Index * tmp_indices   = new Index [cpu_sa.count];

    #pragma omp parallel
    {
      #pragma omp master
    for (Index i=0;i<cnt_buckets;i++)
    {
        if (cpuBuckets[i]+1<cpuBuckets[i+1])
        #pragma omp task
//       SortStringsCountRecursive(SortScope(cpuBuckets[i],cpuBuckets[i+1],LEN_WORD), cpu_sa.offsets,tmp_indices, cpu_sa.buf_data, cpu_sa.offsets[cpu_sa.count]);
       SortStringRadixQuicksort( cpuBuckets[i],cpuBuckets[i+1]-1,LEN_WORD, cpu_sa.offsets, cpu_sa.buf_data);
    }
    #pragma omp taskwait
    }
  delete [] cpuBuckets;
  */
  //delete [] tmp_indices;
   timer1.Notch();
   my_stats.Accumulate("7cpu_part",timer1.GetLastIntervalSeconds()*0.7);
//    CUDA_SAFE_CALL(cudaMalloc((void**) &gpuStack, SIZE_SORT_FRAME*CNT_BUCKETS*sizeof(SortScope)*cntBlocks));

  gpuErrchk(cudaFree(gpuBucketsCnts));
  gpuErrchk(cudaFree(gpuBucketsOffsets));
  return;
/*
  //  byte *gpuBordersBuckets=NULL;
//    Index *gpuBuckets2StageCnts=NULL;
//    Index *gpuBuckets2StageOffs=NULL;
    CUDA_SAFE_CALL(cudaMalloc((void**) &gpuRef, sizeRef+1));
    Index sizeMemSA= (cntLines+1)*sizeof(Index);
    //std::cerr<<"\n\tallocating 2x " <<FormatHelper::SizeToHumanStr(sizeMemSA)<<" for SA\n";
    CUDA_SAFE_CALL(cudaMalloc((void**) &gpuSA, cntLines*sizeof(Index)));
   // std::cerr<<"\tallocating 2x " <<FormatHelper::SizeToHumanStr(sizeMemBuckets)<<" for buckets\n";

   // std::cerr<<"\tallocating    " <<FormatHelper::SizeToHumanStr(SIZE_SORT_FRAME*CNT_BUCKETS*sizeof(SortScope))<<" for stack buffer\n";
    SortScope* gpuStack=NULL;


    CUDA_SAFE_CALL (cudaMemcpy(gpuRef,ref, sizeRef, cudaMemcpyHostToDevice));
    CUDA_SAFE_CALL (cudaMemcpy(gpuSA, sa, cntLines * sizeof(Index), cudaMemcpyHostToDevice));



    //CUDA_SAFE_CALL (cudaMemcpy(gpuBucketsOffsets, cpuBuckets, 2 * sizeof(Index), cudaMemcpyHostToDevice));
  	//std::cout<<"   ####### kernel ########  ";
    CUDA_SAFE_CALL(cudaMemset 	( gpuBucketsCnts,0	,(CNT_BUCKETS+1)*sizeof(Index)));
   // CUDA_SAFE_CALL (cudaMemset 	( gpuBordersBuckets,0	,cntLines));

   //	krnlCalcOffsets<<<1,1>>>(gpuBucketsCnts,gpuBucketsOffsets,CNT_BUCKETS);
   	cntBlocks=128;
   // CUDA_SAFE_CALL(cudaMalloc((void**) &gpuStack, SIZE_SORT_FRAME*CNT_BUCKETS*sizeof(SortScope)*cntBlocks));


//    krnlSortCountRecursive<<<cntBlocksReduceBuckets,cntThreadsPerBlock>>>(LEN_WORD,gpuBucketsOffsets,CNT_BUCKETS,gpuSA2,gpuSA,gpuBordersBuckets,gpuRef,sizeRef );
   // krnlSortStack<<<cntBlocksReduceBuckets,cntThreadsPerBlock>>>(LEN_WORD,gpuBucketsOffsets,CNT_BUCKETS,gpuSA2,gpuSA,gpuBordersBuckets,gpuRef,sizeRef,gpuStack );
     //krnlSortInsertion<<<cntBlocksReduceBuckets,cntThreadsPerBlock>>>(LEN_WORD,gpuBucketsOffsets,CNT_BUCKETS,gpuSA2,gpuSA,gpuRef,sizeRef);
    TestCudaResult();
   // unsigned long long * cpuCountWords=new unsigned long long[(CNT_BUCKETS+1)];
    //CUDA_SAFE_CALL (cudaMemcpy(cpuCountWords, gpuBucketsCnts,cuda-memcheck ./yourApp(CNT_BUCKETS+1) * sizeof(Index), cudaMemcpyDeviceToHost));
    //std::cerr<<"\n";
   // for (int i =0; i<CNT_BUCKETS; i++)
     //   std::cerr<<cpuCountWords[i]<<"\n";а метрической систем

    CUDA_SAFE_CALL (cudaMemcpy(sa, gpuSA2, cntLines * sizeof(Index), cudaMemcpyDeviceToHost));
    //byte  * cpuBordersBuckets= new byte[cntLines];
    //CUDA_SAFE_CALL (cudaMemcpy(cpuBordersBuckets, gpuBordersBuckets, cntLines, cudaMemcpyDeviceToHost));
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    //cudaThreadSynchronize();
    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop); // that's our time!
    std::cerr<<"\tgpu time= "<<elapsedTime/1000<<"s"<<std::endl;
    cudaEventDestroy(start);
    cudaEventDestroy(stop);gpuTempSA
    */

    /*
    #pragma omp parallel for schedule(dynamic)
    for (Index i=0;i<CNT_BUCKETS;i++)
    {
        //std::cerr<<"running task " <<cpuBuckets[i]<<" - "<<cpuBuckets[i+1]<<"\n";
        //#pragma omp task
        if (cpuBuckets[i]+1<cpuBuckets[i+1])
       SortStringsCountRecursive(SortScope(cpuBuckets[i],cpuBuckets[i+1],LEN_WORD), sa,sa2, ref, sizeRef);
    }
    */
    //Index i=0;
    //Index posPrevBucket=0;
    //Index cntUnsorted=0;
  //  Timer timer;
    //timer.Notch();
    /*
#pragma omp parallel shared(sa) shared(sa2)  shared(ref)
{
#pragma omp single
{
    for (i = 0; i<cntLines; i++ )
    {
        if (cpuBordersBuckets[i]==1)
        {
            if ((i-posPrevBucket)>1)
            {cuda-memcheck ./yourApp
               //std::sort(sa+posPrevBucket,sa+i,ComparatorStrings(gpuRef,0,sizeRef));
                //std::cerr<<"unsorted bucket "<<posPrevBucket<<" "<<i<<"\n";
               cntUnsorted++;
               if (cntUnsorted<100)
               {
               //std::cerr<<"starting task "<<cntUnsorted<<"\n";
              // #pragma omp task
               SortStringsCountRecursive(SortScope(posPrevBucket,i,LEN_WORD*2),sa,sa2,ref,sizeRef,NULL);
               }buf_data
               else
               SortStringsCountRecursive(SortScope(posPrevBucket,i,LEN_WORD*2),sa,sa2,ref,sizeRef,NULL);
            }

            posPrevBucket=i;
        }
    }
    if ((i-posPrevBucket)>1)
    {
//        std::sort(sa+posPrevBucket,sa+i,ComparatorStrings(ref,0,sizeRef));
               #pragma omp task
               SortStringsCountRecursive(SortScope(posPrevBucket,i,LEN_WORD*2),sa,sa2,ref,sizeRef,cpuBordersBuckets);
    }
    #pragma omp taskwait
    }
}
*/
//timer.Notch();
//std::cerr<<"final sorting done in "<<timer.GetLastIntervalHumanReadable()<<"\n";
//std::cerr<<cntUnsorted<<" unsorted buckets detected\n";

//    CUDA_SAFE_CALL(cudaFree(gpuRef));
//	CUDA_SAFE_CALL(cudaFree(gpuSA));
//	CUDA_SAFE_CALL(cudaFree(gpuSA2));
//	CUDA_SAFE_CALL(cudaFree(gpuStack));

  //  delete [] cpuBordersBuckets;
  //  delete [] sa2;
   // delete [] cpuBuckets;
}

void SortStrigns(StringArray & sa)
{
     std::sort(sa.offsets, sa.offsets+sa.count, ComparatorStringArray(sa))  ;
}

void DoGPUStringSoprt(StringArray & sa)
{
  GPUStringArray gpuSA;
  gpuSA.Allocate(sa);
  gpuSA.CopyToGPU(sa);
  RunKrnlSort(gpuSA,sa);
  //gpuSA.CopyFromGPU(sa);
  gpuSA.Free();
}

int main(int argc, char* argv[])
{
  PrintProperties(0);
  Index len_key=32;
  Index cntKeys=14000000LL;
  Index iter=0;
  cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);   
   //for (cntKeys=1ll*1000*100;cntKeys<20ll*1000*1000;cntKeys+=1ll*1000*500)
  //for (len_key=8;len_key<150;len_key+=8)
  {
    std::cerr<<"generating "<< cntKeys<<" rnd strings on iter "<<++iter<<"\n";
    StringArray sa=GenerateRandomStrings(cntKeys,len_key);
    std::cerr<<"dataset size = "<<FormatHelper::SizeToHumanStr(cntKeys*len_key)<<"\n";
    sa.PrintHead();
    
    my_stats.Accumulate("1cnt_keys",cntKeys);
    GPUStringArray gpuSA;
//    cudaHostRegister  (sa.buf_data, sa.offsets[sa.count],  cudaHostRegisterPortable) ;
    krnlEmpty<<<1,1>>>();
    double time_run=Timer::GetSeconds();
    Timer timer1;
    timer1.Notch();
   // StringArray sa_trimmed=sa.Trim();
    timer1.Notch();
    
    gpuSA.Allocate(sa);
   
    std::cerr<<"allocated\n";
    gpuSA.CopyToGPU(sa);
 
    //my_stats.Accumulate("2atrim",timer1.GetLastIntervalSeconds());
    
    timer1.Notch();
    my_stats.Accumulate("2bcopy",timer1.GetLastIntervalSeconds());
    
    RunKrnlSort(gpuSA,sa);
    
    time_run=Timer::GetSeconds()-time_run;
    time_run=gtime;
    std::cerr<<"mtime  "<<time_run<<"\n";
    double keyPerSec =  1.2*static_cast<double>(cntKeys)/time_run;
    std::cerr << "Keys per second = " <<keyPerSec<< "\n";
    WriteStatsToFile("GPU_hybryd",cntKeys,keyPerSec);
    WriteStatsToFile("keys",len_key,keyPerSec);
 // cudaHostUnregister(sa.buf_data);
  //StatWriter sw_scaling=StatWriter();
  //sw_scaling.Accumulate("len_key",len_key);
  //sw_scaling.Accumulate("time",time_run);
    //gpuSA.CopyFromGPU(sa_trimmed);
    gpuSA.Free();    
    sa.PrintHead();
    std::string name_experiment="";
    name_experiment=name_experiment+"_alph"+FormatHelper::ConvertToStr(LENGTH_ALPHABET);
    name_experiment=name_experiment+"_klen"+FormatHelper::ConvertToStr(len_key);
    my_stats.Write(name_experiment+".perf_brkdown");

    std::cerr<<"\n-----------------\n";
    delete[] sa.offsets;
    delete[] sa.buf_data;
//    delete[] sa_trimmed.offsets;
//    delete[] sa_trimmed.buf_data;
  }
  return 0;
}
