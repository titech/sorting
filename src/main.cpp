#include <iostream>
#include <algorithm>
#include <cstdio>

#define LEN_ALPHABETH 256;

#include "sortutils.hpp"

#include "selection.cpp"
#include "quick_my.cpp"


void RunExperiments(Index size)
{
    datatype * array = new datatype [size];
    int seed=1;
//    RunExperiment(SelectionSort<datatype>,array,size,"selection",seed);
//    RunExperiment(InsertionSort<datatype>,array,size,"insertion",seed);
    RunExperiment(QuickSort<datatype>,array,size,"quicksort_64b",seed);
    delete [] array;
}

int main(int argc, char *argv[])
{
    Index size=1LL*100*100*5;
    for (Index size=1ll*1000*1000;size<20ll*1000*1000;size+=1ll*1000*100)
        RunExperiments(size);
    return 0;
}