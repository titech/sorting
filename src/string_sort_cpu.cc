#include <iostream>
#include <algorithm>
#include <fstream>
#include <iomanip>
#if _OPENMP
#endif
#include <omp.h>
#ifdef COMMON_CILKH
#include <mtbb/task_group.h>
//#include <common.cilkh>
#include <tpswitch/tpswitch.h>
#else
#define spawn ()
#define mk_task_group ()
#define create_task0(E) (E)
#define wait_tasks  ()
#endif

#include "sortutils.hpp"
#include "string_array.hpp"

unsigned int num_threads=0;

#ifdef DO_LOI
#include "loi.h"

struct loi_kernel_info sort_kernels = {
        7,              // 5 kernels in total
        7,              // 5 phases: each kernel is one phase
        {"Histogram_Core", "Reduction_Core", "PrefixSum_Core", "OffsetsPerThreadPointers_Core", "MoveKeys_Core", "Copy_Core", "Recursion_Core"}, // Name of the two kernels
        {"Histogram", "Reduction", "PrefixSum", "OffsetsPTP", "MoveKeys", "Copy", "Recursion"},       // There is only one phase
        {1<<HISTOGRAM, 1<<REDUCTION , 1<<PREFIXSUM , 1<<OFFSETSPTP , 1<<MOVEKEYS, 1<<COPY, 1<<RECURSION}, // ugly way to map the kernels
        };

#endif



#if TO_MTHREAD_NATIVE
#include <myth_if_native.h>
#endif

#include "string_sort_quick.hpp"
#include "string_sort_radix.hpp"

extern Index cnt_tasks_total;
extern std::atomic<unsigned int> pos_taskprofile_buf;
extern Snapshot log_tasks[SIZE_TASKPROFILE_BUF];

unsigned int cnt_tasks=16;

template <typename T>
void delete_safe(T &p)
{
    if (p==NULL)
    {
        std::cerr<<"pointer already freed!";
        return;
    }
    delete [] p;
    p=NULL;
}

void DumpTaskProfile()
{
  std::ofstream myfile;
  myfile.open ("parallel.profile");
  double start_time=log_tasks[0].timestamp;
   myfile <<"time\ttasks\twaiting\n";
  for (unsigned int i=0;i<pos_taskprofile_buf;i++)
  {
   myfile << std::setprecision(16) << log_tasks[i].timestamp-start_time<<"\t";
   myfile <<log_tasks[i].cnt_tasks<<"\t";
   myfile <<log_tasks[i].cnt_tasks_waiting<<"\n";
  }
  myfile.close();
}

void validate(const StringArray &sa)
{
    Index cnt_out_of_order=0;
    for (Index i=0;i<sa.count-1;i++)
    if (memcmp(sa.buf_data+sa.offsets[i],sa.buf_data+sa.offsets[i+1],10)>0)
    {
    cnt_out_of_order++;
   // std::cerr<<sa.offsets[i]<<"\n";
    }
    std::cerr<<"out of order entries = "<<cnt_out_of_order<<"\n";
}

int main(int argc, char* argv[])
{
  if (argc < 2) {
   	std::cerr << "Usage: " << argv[0] << " cnt_keys  len_key" << std::endl;
   	return 1;
  }


  StatWriter &my_stats=StatWriter::GetInstance();

  Index cnt_keys=FormatHelper::StrToInt(argv[1]);
  Index len_key=FormatHelper::StrToInt(argv[2]);

  std::cerr<<"generating rnd strings \n";
  StringArray sa=GenerateRandomStrings(cnt_keys,len_key);
  std::cerr<<"dataset size = "<<FormatHelper::SizeToHumanStr(cnt_keys*len_key)<<"\n";
  sa.PrintHead();

#if DO_LOI
  loi_init(); // calc TSC freq and init data structures
  printf(" TSC frequency has been measured to be: %g Hz\n", (double) TSCFREQ);
#endif

  Index * tmp_indices   = new Index [sa.count];
  //const Index size_buf = sa.offsets[sa.count];
  #ifdef MIC
  Index * sa1= sa.offsets;
  byte * ref= sa.buf_data;
  const Index cnt_lines = sa.count;
  #pragma offload target(mic:MIC_DEV) nocopy(sa1:length(sa.count) alloc_if(1) free_if(0))  nocopy(tmp_indices:length(sa.count) alloc_if(1) free_if(0)) nocopy(ref:length(sa.offsets[sa.count]) alloc_if(1) free_if(0))
  {}
  #endif

  #ifdef MIC
  #pragma offload target(mic:MIC_DEV) // first call to device might take some time
  #endif

//#if TO_MTHREAD_NATIVE
//  myth_init();
//#endif
  {
#ifdef _OPENMP
    #pragma omp parallel
    {
      #pragma omp master
      {
          num_threads=omp_get_num_threads();
//          num_threads=244;
      }
    }
#else
   num_threads = 1;
#endif
  }
 // sleep(1);
   //num_threads=32;
  //printf ("num threads=%d\n", num_threads );
  my_stats.Accumulate("keys",cnt_keys);
 //init_runtime(&argc, &argv);
  std::string name_algo="";



  #ifdef MIC
  #pragma offload target(mic:MIC_DEV) inout(sa1:length(sa.count)  alloc_if(0) free_if(0)) inout(tmp_indices:length(sa.count)  alloc_if(0) free_if(0)) in(ref:length(size_buf) alloc_if(0) free_if(0))
  #endif
  //SortStringsCountAutomataOMP(0,cnt_lines,0, sa1,tmp_indices,ref,size_buf);


  //SortStringsCountAutomataOMP(0,sa.count,0, sa.offsets, tmp_indices, sa.buf_data, sa.offsets[sa.count]);
//  SortStringsElimitation(SortScope(0,sa.count,0), sa.offsets, tmp_indices, sa.buf_data, sa.offsets[sa.count]);
  //name_experiment=name_experiment+"3WAYC_TASK";
  /*
  #pragma omp parallel
  {
  #pragma omp single
  {
  */
  //num_threads=omp_get_num_threads();
  //name_experiment=name_experiment+"MultiquickCountingTasks";
//  for (unsigned int cnt_b=1;cnt_b<244;cnt_b+=10)
 // {

#if TO_MTHREAD_NATIVE
  if(getenv("MYTH_HACK"))
     myth_init_withparam(atoi(getenv("MYTH_HACK")), 131072);
#endif
//    cnt_tasks=cnt_b;
    std::cerr<<"cnt_tasks = " <<cnt_tasks<<"\n";
 double time_run=Timer::GetSeconds();
#if DAG_RECORDER == 2
    dr_start(0);
#endif


  name_algo=std::string("AutomataOMP");//+FormatHelper::ConvertToStr(LEN_WORD);
  SortStringsCountAutomataOMP(0,sa.count,0, sa.offsets, tmp_indices, sa.buf_data); // , sa.offsets[sa.count]);
 //SortStringRadixQuicksortCountingTasks(0,sa.count,0, sa.offsets,tmp_indices,sa.buf_data);

  //name_experiment=name_experiment+"MultyCountingSerial";
  //SortStringRadixQuicksortCounting(0,sa.count,0, sa.offsets,tmp_indices,sa.buf_data);

  //name_experiment=name_experiment+"MultySwapSerial";
  //SortStringRadixQuicksort(0,cnt_lines,0, sa1,ref);

  //SortStringsCountRecursive(SortScope(0,sa.count,0), sa.offsets, tmp_indices, sa.buf_data, sa.offsets[sa.count]);
  //SortStringsCountAutomataOffload(0,sa.count,0, sa.offsets, tmp_indices, sa.buf_data, sa.offsets[sa.count]);
  //SortStringsCountRecursiveTasks(SortScope(0,sa.count,0), sa.offsets, tmp_indices, sa.buf_data, sa.offsets[sa.count]);
  /*
  }
  }
  */


  time_run=Timer::GetSeconds()-time_run;

#if DAG_RECORDER == 2
    dr_stop();
    dr_dump();
#endif

  //std::cerr<<"total tasks spawned = "<<cnt_tasks_total<<"\n";

  //#ifdef MIC
  //#pragma offload target(mic:MIC_DEV) nocopy(sa1:length(sa.count) alloc_if(0) free_if(1))  nocopy(tmp_indices:length(sa.count) alloc_if(0) free_if(1)) nocopy(ref:length(sa.offsets[sa.count]) alloc_if(0) free_if(1))
  //#endif
  //{  }
  std::cerr<<"mtime  "<<time_run<<"\n";
  double keys_per_sec = static_cast<double>(cnt_keys)/time_run;
  std::cerr << "Keys per second = " <<keys_per_sec<< "\n\n";
 // }
  StatWriter &sw_scaling=StatWriter::GetInstance();
//  sw_scaling.Accumulate("threads",num_threads);
//  sw_scaling.Accumulate("time",time_run);
  std::string name_compiler="";
  #ifdef COMPILER
  name_compiler = FormatHelper::ConvertToStr(COMPILER);
  #endif

  std::string name_file_out=GetExperimentName(LENGTH_ALPHABET,len_key,name_compiler,name_algo,num_threads);

  WriteStatsToFile(name_file_out+std::string(".thrghpt"), cnt_keys, keys_per_sec);
//  my_stats.Write(name_experiment+".perf_brkdown");
  //sw_scaling.Write("scaling");

// return 0;

  sa.PrintHead();
  std::cerr<<"\n-----------------\n";
  validate(sa);
  delete_safe(tmp_indices);
  delete_safe(sa.offsets);
  delete_safe(sa.buf_data);
#ifdef DO_LOI
  loi_statistics(&sort_kernels, num_threads);
#endif
  //DumpTaskProfile();
  return 0;
}
