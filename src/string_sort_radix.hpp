#ifndef H_SORT_RADIX
#define H_SORT_RADIX

#include "utils/types.hpp"

struct SortScope
{
    Index start;
    Index end;
    unsigned int depth;
    #ifdef MIC
    __attribute__((target(mic))) SortScope(Index _start,Index _end,unsigned int _depth):start(_start),end(_end),depth(_depth){}
    #else
    SortScope(Index _start,Index _end,unsigned int _depth):start(_start),end(_end),depth(_depth){}
    #endif
};


    #ifdef MIC
void __attribute__((target(mic))) SortStringsCountRecursive(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef);
    #else
void SortStringsCountRecursive(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef);
    #endif

void SortStringsCountRecursiveOMP(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef);


#ifdef MIC
void __attribute__((target(mic))) SortStringsCountAutomataOMP(const Index start, const Index end ,const Index depth, Index * sa,Index * saTemp, byte * ref,const Index refsize);
#else
void SortStringsCountAutomataOMP(const Index start, const Index end ,const Index depth, Index * sa,Index * saTemp,const  byte * ref);
#endif

void SortStringsCountAutomataOffload(const Index start, const Index end, const Index depth, Index * sa,Index * saTemp, byte * ref,const Index refsize);


    #ifdef MIC
void __attribute__((target(mic))) SortStringsCountRecursive(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef);
    #else
void SortStringsCountRecursiveTasks(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef);
    #endif

#endif

void SortStringsElimitation(const SortScope scope, Index * sa,Index * sa2, const byte * ref,Index sizeRef);
