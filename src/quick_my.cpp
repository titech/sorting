template <class T>
void QS(T *array, Index start, Index end)
{
	T pivot = array[(start+end)/2];
	Index i = start, j = end;
	
	do  {
		while (array[i] < pivot) i++;
		while (array[j] > pivot) j--;
		if (i<=j) {
			std::swap(array[i],array[j]);
			i++;
			j--;
		}
	} while (i<=j);
	std::cerr<<"i = "<<i<<"\n";
	std::cerr<<"j = "<<j<<"\n";
	if (i<end)
		QS(array,i,end);
	if (start<j)
		QS(array,start,j);
}

template <class T>
void QS3(T *array, Index start, Index end)
{
	Index lo=start, hi=end;
	Index i=start;
	T pivot = array[i];
	while (i<=hi) {
		if (array[i]<pivot) 
		{
			std::swap(array[i],array[lo]);
			lo++; 
			i++;
		} else
		if (array[i]>pivot)
		{
			std::swap(array[i],array[hi]);
			hi--;
		} else
		if (array[i]==pivot) i++;
	} 
	if (start<lo) QS3(array,start,lo-1);
	if (hi<end) QS3(array,hi+1,end);
}


template <class T>
void QS3_count_old(T *array,T *array_tmp, Index start, Index end)
{
	if (start+1>=end) return;
//	std::cerr<<"sorting from "<<start<<" to "<<end<<"\n";
	Index i=start;
	T pivot = array[i];
    Index cnt_less=0;
    Index cnt_eq=0;
    Index cnt_great=0;
  //  #pragma omp parallel for reduction(+ : cnt_less,cnt_eq) 
    for (Index i=start;i<end;i++)
    {
      if (array[i]<pivot) cnt_less++;
        else if (array[i]==pivot) cnt_eq++;
    }
    cnt_great=end-start-cnt_less-cnt_eq;

//    std::cerr<<"  less = "<<cnt_less<<"\n";
//    std::cerr<<"  eq   = "<<cnt_eq<<"\n";
//    std::cerr<<"  gret = "<<cnt_great<<"\n";

    Index posLess=start;
    Index posEq=posLess+cnt_less;
    Index posGreat=posEq+cnt_eq;
    for (Index i=start;i<end;i++)
    {
      if (array[i]<pivot) 
        array_tmp[posLess++]=array[i];
      else 
      if (array[i]==pivot) 
        array_tmp[posEq++]=array[i];
      else
        array_tmp[posGreat++]=array[i];
    }

    for (Index i=start;i<end;i++)
      array[i]=array_tmp[i];
	if (cnt_less>1)
   		QS3_count(array, array_tmp, start, start+cnt_less );		
   	if (cnt_great>1) 	
   		QS3_count(array, array_tmp, start+cnt_less+cnt_eq, start+cnt_less+cnt_eq+cnt_great);	
}


template <class T>
void QS3_count(T *array,T *array_tmp, Index start, Index end)
{
	if (start+1>=end) return;
//	std::cerr<<"sorting from "<<start<<" to "<<end<<"\n";
	Index i=start;
	T pivot = array[i];
    Index cnt_less=0;
    Index cnt_eq=0;
    Index cnt_great=0;
    unsigned int cnt_tasks=12*2;
if ((end-start)<=10000)
{
    //#pragma omp parallel for reduction(+ : cnt_less,cnt_eq) 
    for (Index i=start;i<end;i++)
    {
      if (array[i]<pivot) cnt_less++;
        else if (array[i]==pivot) cnt_eq++;
    }
}
else
{
    Index * cnts_less = new Index[cnt_tasks];
    Index * cnts_eq = new Index[cnt_tasks];
    for (unsigned int i=0;i<cnt_tasks;i++)
    {
    	cnts_less[i]=0;
    	cnts_eq[i]=0;
    }
    double cnt_lines_per_task=(double(end-start))/cnt_tasks;
    for (unsigned int idTask=0;idTask<cnt_tasks;idTask++)
    {
      #pragma omp task
      [idTask,start,cnt_lines_per_task,pivot,&array,&cnts_less,&cnts_eq]()
      {
      	Index pos_task_begin= start+(cnt_lines_per_task*idTask);
     	Index pos_task_end  = start+(cnt_lines_per_task*(idTask+1));
        for (Index i=pos_task_begin;i<pos_task_end;i++)
    	{
      	if (array[i]<pivot) cnts_less[idTask]++;
	        else if (array[i]==pivot) cnts_eq[idTask]++;
	    }
      }();
    }
    #pragma omp taskwait
    for (unsigned int i=0;i<cnt_tasks;i++)
    {
    	cnt_less+=cnts_less[i];
    	cnt_eq+=cnts_eq[i];

	}
    delete [] cnts_less; 
    delete [] cnts_eq; 
 }
    cnt_great=end-start-cnt_less-cnt_eq;

//    std::cerr<<"  less = "<<cnt_less<<"\n";
//    std::cerr<<"  eq   = "<<cnt_eq<<"\n";
//    std::cerr<<"  gret = "<<cnt_great<<"\n";

    Index posLess=start;
    Index posEq=posLess+cnt_less;
    Index posGreat=posEq+cnt_eq;
    for (Index i=start;i<end;i++)
    {
      if (array[i]<pivot) 
        array_tmp[posLess++]=array[i];
      else 
      if (array[i]==pivot) 
        array_tmp[posEq++]=array[i];
      else
        array_tmp[posGreat++]=array[i];
    }

    for (Index i=start;i<end;i++)
      array[i]=array_tmp[i];

//   if (cnt_less>100) 
//   	{
//	   #pragma omp task
//   		QS3_count(array, array_tmp, start, start+cnt_less );
//   	}
//	   	else 
   		if (cnt_less>1)
   		QS3_count(array, array_tmp, start, start+cnt_less );		
//   if (cnt_great>100) 
//   	{
//	   #pragma omp task
//   		QS3_count(array, array_tmp, start+cnt_less+cnt_eq, start+cnt_less+cnt_eq+cnt_great);
//   	}
  // 	else
	   	if (cnt_great>1) 	
   		QS3_count(array, array_tmp, start+cnt_less+cnt_eq, start+cnt_less+cnt_eq+cnt_great);	
//   	#pragma omp taskwait
}

template <class T>
void QuickSort(T * array,Index size)
{
	//QS3(array,0,size-1);
	//return;
	T* array_tmp = new T[size];
	#pragma omp parallel
	{
		#pragma omp single
		{
		#pragma omp task
		QS3_count(array,array_tmp,0,size);
		}
	}
	delete[] array_tmp;
}
