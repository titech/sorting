#define SEED 1

#include <cstring>
#include <iostream>

#include "sortutils.hpp"
#include "string_array.hpp"

#ifdef MIC
Index __attribute__((target(mic))) GetNumBuckets(const Index size_alphabet,const Index len_word)
#else
Index GetNumBuckets(const Index size_alphabet,const Index len_word)
#endif
{
  if (len_word == 0) return 1;
  if (len_word == 1) return size_alphabet;
  return size_alphabet * GetNumBuckets  (size_alphabet, len_word-1);
}


void StringArray::PrintString(Index idx)
    {
    std::cerr<<offsets[idx]<<":\t";
    	for (Index idxSymbol=offsets[idx]; idxSymbol<offsets[idx]+15; idxSymbol++)
    	{
        	std::cerr<<static_cast<int>(buf_data[idxSymbol])<<" ";
	    }
    }

void StringArray::PrintHead()
    {
    	for (Index i=0;i<10;i++)
    	//for (Index i=0;i<count;i++)
    	{
    		PrintString(i);
    		std::cerr<<"\n";
    	}
    }

StringArray StringArray::Trim()
    {
      StringArray a;
      a.count=count;
      a.offsets = new Index[count+1];
      a.buf_data = new byte[count*16];
      #pragma omp parallel for
      for (Index i=0;i<count;i++)
      {
        a.offsets[i]=i*16;
        std::memcpy(a.buf_data+i*16, buf_data+offsets[i],8);
        *(reinterpret_cast<Index *>(a.buf_data+i*16+8))=offsets[i];
      }

      a.offsets[count]=count*16;
      return a;
    }



bool ComparatorStringArray::operator() (Index i,Index j)
 	{
  		//return (i<j);
  		//std::cerr<<"comparing strings "<<i<<" and "<<j<<"\n";
  		//Index lenScan=std::min(sa.offsets[i+1]-sa.offsets[i],sa.offsets[j+1]-sa.offsets[j]);
  		Index lenScan=100;
  		//std::cerr<<"scan length  is  "<<lenScan<<"\n";

  		for (Index pos=0;pos<lenScan;pos++)
  		{
  			if (sa.buf_data[i+pos]<sa.buf_data[j+pos]) return true;
  			if (sa.buf_data[i+pos]>sa.buf_data[j+pos]) return false;
  		}
  		return false;
	}



StringArray GenerateRandomStrings(Index cntKeys, Index lenKey)
{
    srand(SEED);
    StringArray resultStrings;
    resultStrings.offsets= new Index [cntKeys+1];
    resultStrings.buf_data = new byte [cntKeys*lenKey];//check if we need extra byte for 0
    resultStrings.count=cntKeys;
    FillRNDNums(resultStrings.buf_data, resultStrings.buf_data+cntKeys*lenKey, 1, LENGTH_ALPHABET);
    for (Index idString=0;idString<=cntKeys;idString++)
	     resultStrings.offsets[idString]=lenKey*idString;
    return resultStrings;
}

