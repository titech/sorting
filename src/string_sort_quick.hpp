#ifndef H_STRING_QUICK
#define H_STRING_QUICK

#include "utils/types.hpp"
#include <atomic>
#define SIZE_TASKPROFILE_BUF 640000


struct Cnts
{
  Index less;
  Index eq;
  Index great;
};

struct Snapshot
{
    double timestamp;
    int cnt_tasks;
    int cnt_tasks_waiting;
};

void SortStringRadixQuicksort( const Index start, const Index end, const Index depth, Index * sa, const byte * ref);
void SortStringRadixQuicksortCounting( const Index start, const Index end, const Index depth, Index * sa, Index *sa_tmp, const byte * ref);

void PrintSpace(Index c);

void kernelCount(Index start, Index end, Index depth,const Index * sa, const byte * ref,const byte pivot, Cnts * a_cnts, unsigned int id_task);
void kernelMove(Index start, Index end, Index depth,Index * sa, Index * sa_tmp, const byte * ref,const byte pivot, Cnts * a_cnts, Cnts positions, unsigned int id_task);
void SortStringRadixQuicksortCountingOMP( const Index start, const Index end, const Index depth, Index * sa, Index *sa_tmp, const byte * ref, Index r);
void SortStringRadixQuicksortCountingTasks( const Index start, const Index end, const Index depth, Index * sa, Index *sa_tmp, const byte * const ref);

#endif
