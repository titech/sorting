#include <fstream>
#include <string>
#include <map>
//#include <random>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <sys/stat.h>
#include "utils/timer.hpp"
#include "utils/types.hpp"
#define SEED 1

//void FillRNDNums(unsigned long int * a, Index size, int seed);

template<class RandomAccessIterator>
void FillRNDNums(RandomAccessIterator first, RandomAccessIterator last, const int seed, const Index range)
{
    srand(seed);
   // srand(time(NULL));

  //std::default_random_engine generator;

  //std::discrete_distribution<int> distribution {1,1,1,1};

    for (RandomAccessIterator i=first;i<last;i++)
    //*i = distribution(generator);
       *i=(static_cast<double>(range-1)*rand()/(RAND_MAX+1.0))+1;
       //*i=0;
}

/*void FillRandom()
{
    std::random_device rd;
//  std::mt19937 gen(rd()); //rd is a seed?
    std::mt19937 gen(SEED);
    //double mean=0;
    //double stddev=2;
    //std::normal_distribution<> d(mean,stddev);
    //std::round(d(gen));
    //double probabilities[] = { 0.5, 0.1, 0.1, 0.1, 0.1, 0.1 };
    //std::discrete_distribution<int> dist(probabilities);
    std::discrete_distribution<int> distribution {2,2,1,1};
}
*/

std::string GetHostName();
std::string GetExperimentName(long len_alphabet,long len_key,std::string name_compiler,std::string name_algo,long num_threads);

void WriteStatsToFile(std::string filename,Index cntReads, double keyPerSec);


class StatWriter  //implement singleton here
{
    std::map<std::string,double> stats;
    StatWriter(){};
public:
    void Write(std::string name_experiment);
    void Accumulate(std::string name_prop, double value);
    static StatWriter& GetInstance()
   {
      static StatWriter instance;
      return instance;
   }
};

template <typename T>
void Print(T * array, Index size)
{
    for (int i=0;i<100;i++)
        std::cout<<array[i]<<" ";
    std::cout<<"\n";
}


/*
template <typename F>
double MeasureTime(F f,int * array, Index cnt_keys)
{
    clock_t clockStart, clockStop;
    tms tmsStart, tmsStop;
    clockStart = times(&tmsStart);
    f(array,cnt_keys);
    clockStop = times(&tmsStop);
    std::cerr<<"clock diff "<<clockStop-clockStart<<"\n";
    double time_run=(static_cast<double>(clockStop - clockStart))/(sysconf(_SC_CLK_TCK)) ;
    return time_run;
}
*/


template <typename F, typename T>
double MeasureTime2(F f,T * array, Index cnt_keys)
{
    double seconds=Timer::GetSeconds();
    f(array,cnt_keys);
    seconds=Timer::GetSeconds()-seconds;
    std::cerr<<"mtime  "<<seconds<<"\n";
    return seconds;
}



template <typename F, typename T>
void MeasureAndLog(F f,T * array, Index cnt_keys,std::string name_exp)
{
    double time_run = MeasureTime2(f,array,cnt_keys);
    double keyPerSec =  static_cast<double>(cnt_keys)/time_run;
    std::cerr << "Done in " << time_run<< " seconds\n";
    std::cerr << "Keys per second = " <<keyPerSec<< "\n";
    WriteStatsToFile(name_exp,cnt_keys,keyPerSec);
}

template <class T>
bool Validate(T *array, Index count)
{
    for (Index i=1;i<count;i++)
        if (array[i]<array[i-1]) return false;
    return true;
}


template <typename F,typename T>
void RunExperiment(F f,T * array, Index cnt_keys,std::string name_exp, int seed)
{
    std::cerr<<"creating array of random numbers \n";
    std::cerr<<"size = "<<static_cast<double>(cnt_keys)/(1024*1024*1024)<<"G \n";
    FillRNDNums(array,array+cnt_keys,seed,65536);
    Print(array,cnt_keys);
    std::cerr<<"\nDoing "<<name_exp<<"\n";

    MeasureAndLog(f,array,cnt_keys,name_exp);
    Print(array,cnt_keys);
    std::cerr<<"\n";
    if (Validate(array,cnt_keys))
        std::cerr<<"validation ok\n";
    else
        std::cerr<<"validation FAILED!!!!!!!!!!!!!!!!!!!!!!!!\n";
}
