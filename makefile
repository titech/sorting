my: mth_exes
	scp exes/sort_2_im_mth mic0:/home/blackbird/

#LIBS := $(LIBPATH) -lrt  

#CXX	:= g++
#CXX	:= icc
#CXX	:= scorep --nocuda --noonline-access g++

#CFLAGS := -Wall -Wextra -ggdb -g3  -O2 -D COMPILER=\\\"$(CXX)\\\"
#CFLAGS := -Wall -Wextra -ggdb -g3  -O2 -D COMPILER=\"scorep\"
#-fdump-tree-optimized="optimized"

#CFLAGS := $(CFLAGS) -fopenmp #gnu openmp support 
#CXXFLAGS := $(CFLAGS) -std=c++11 


#-----------------------------PARALLEL2------------------------------
include makefile.inc

# -------------------
# if you include this file from your Makefile,
# set this in yours before include
#
#icc_dir ?= $(HOME)/opt/intel/composer_xe_2013.5.192
icc_dir ?= /opt/intel/composer_xe_2013_sp1.1.106

default_all : serial_exes omp_exes tbb_exes mth_exes qth_exes 

parameters?=dag_recorder
dag_recorder?=0

#src_dir?=$(parallel2_dir)/apps/sort
src_dir?=src
exe_prefix ?= exes/sort
obj_dir ?= o

papi ?= 0

opts += -Wall -Wextra -ggdb -g3 --std=c++11 -O2 -D COMPILER=\\\"taura\\\" 
opts += -DDAG_RECORDER=%(dag_recorder)s
cc_opts := $(opts)
link_opts := -lm -lrt

# sources to be compiled
cc_srcs := src/string_sort_quick.cc src/string_sort_cpu.cc src/string_array.cc src/sortutils.cc
cpp_srcs := 

to_serial_link_opts := $(link_opts)
to_omp_link_opts := $(link_opts)
to_tbb_link_opts := $(link_opts)
to_cilk_link_opts := $(link_opts)
to_mth_link_opts := $(link_opts)
to_mthpth_link_opts := $(link_opts)
to_qth_link_opts := $(link_opts)
to_nanox_link_opts := $(link_opts)

include $(parallel2_dir)/sys/src/tools/makefiles/compile.mk
