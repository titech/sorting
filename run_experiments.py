#!/usr/bin/env python

import os
import sys
import socket
import shutil
import socket
from subprocess import *
from time import gmtime, strftime, clock, time

def sizeof_fmt(num):
    for x in ['','K','M','G','T']:
        if num < 1024.0:
            return "%3.1f%s.fa" % (num, x)
        num /= 1024.0

def secondsToStr(t):
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll,b : divmod(ll[0],b) + ll[1:],
            [(t*1000,),1000,60,60])

cntKeysInitial=10000
cntKeysMax=20000000
cntKeysIncrement=100000
len_key=20
cntRepeats=1;
dirData="/tmp/gendata/"

def prepareEnvironment():
	if not os.path.exists(dirData):
	    os.makedirs(dirData)
	os.environ['LD_LIBRARY_PATH']=os.getenv('LD_LIBRARY_PATH', '')+"/opt/cuda/tk5.0/lib64/:"
	#print "LD=",os.environ['LD_LIBRARY_PATH']


def conductExperiment(nameLaunch):
	timeStart=time()
	print "writing results to ",nameLaunch;
	fileOut= open(nameLaunch+".data",'w')
	shutil.copy2('../bin/cuda_ssort', '../bin/'+nameLaunch)
	cntReads=cntReadsInitial
	while cntReads<cntMaxReads:
		nameFile=os.path.join(dirData,"qry_seed1_"+sizeof_fmt(cntReads))
		print "generating test file of ",cntReads, "reads"
		p =  Popen(['./generate/generate',nameFile,str(cntReads)],stdout=PIPE,stderr=PIPE)
		output=p.communicate()
		if p.wait() != 0:
			print "There were some errors"
			print output[1]
			quit(-1)
		print "running test on ",nameFile
		p = Popen(["../bin/"+nameLaunch,nameFile],stdout=PIPE,stderr=PIPE)
		output=p.communicate()
		if p.wait() != 0:
			print "There were some errors"
			print output[1]
			quit(-1)
		os.remove(nameFile)
		#print "out:",output[0]
		percentsDone=100*(cntReads*cntReads)/(cntMaxReads*cntMaxReads);
		timeCurrent=time()
		timeETA=(timeCurrent-timeStart)*(cntMaxReads*cntMaxReads)/(cntReads*cntReads)
		print percentsDone, "% done,\ttime spent=",secondsToStr(timeCurrent-timeStart),",\testimated time=",secondsToStr(timeETA),",\ttime left=",secondsToStr(timeETA-timeCurrent+timeStart)
		print 
		fileOut.write(str(cntReads))
		fileOut.write(", ")
		fileOut.write(output[0])
		cntReads=cntReads+cntReadsIncrement
	fileOut.close()
	os.remove('../bin/'+nameLaunch)




#nameLaunch="./string_sort_cpu"
nameLaunch="numactl"
name_binary_original="./string_sort_cpu"
name_binary=""

def prepare_binary():
	global name_binary
	name_binary=name_binary_original
	if socket.gethostname().find('.')>=0:
		name_binary+=socket.gethostname().split(".")[0]
	else:
		name_binary+=socket.gethostname()
	name_binary+=strftime("_%Y%b%d_%H%M", gmtime())
	shutil.copy2(name_binary_original, name_binary)

def clean_binary():
	os.remove(name_binary)

def run_app(cnt_keys,len_key,cnt_threads):
	os.environ['OMP_PLACES'] = "threads"
	#os.environ['OMP_NUM_THREADS'] = str(cnt_threads)
#	os.environ['OMP_NUM_THREADS'] = str(cnt_threads)
	print (name_binary)
	p = Popen([nameLaunch,"-iall",name_binary,str(cnt_keys),str(len_key)],stdout=PIPE,stderr=PIPE)
#	p = Popen([nameLaunch,str(cnt_keys),str(len_key)],stdout=PIPE,stderr=PIPE)
	output=p.communicate()
	if p.wait() != 0:
		print "There were some errors"
		print output[1]
		quit(-1)
	print "out:",output[0]


def run_simple():
	cnt_threads=12;
	for cnt_keys in range (1000, cntKeysMax, cntKeysIncrement):
		print cnt_keys
		for i in range(cntRepeats): run_app(cnt_keys,len_key,cnt_threads)

def measure_scaling():
	cnt_keys=10000000;
	for cnt_threads in range (1,13):
		print cnt_threads
		run_app(cnt_keys,len_key,cnt_threads)

prepare_binary()
#prepareEnvironment()
#conductExperiment(nameLaunch)
run_simple()
#measure_scaling()

clean_binary()